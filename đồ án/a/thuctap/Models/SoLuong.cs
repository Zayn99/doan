﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace thuctap.Models
{
    public class SoLuong
    {
        public int Quantity { get; set; }
        public int ID_Product_Color { get; set; }
        public int ID { get; set; }
        public string Color { get; set; }
        public string Image { get; set; }
        public string Size { get; set; }
    }
}