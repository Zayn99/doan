﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using thuctap.Models;
namespace thuctap.Controllers
{
    public class _clientLoginController : Controller
    { 
        bangiayDataContext db = new bangiayDataContext();
        // GET: _clientLogin
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(Customer tk)
        {
            string email = Request["Email"];
            string password = Request["PassWord"];
            tk = db.Customers.Where(m => m.Email == email && m.Password == password && m.Status == "Active").SingleOrDefault();
            Customer tk1 = db.Customers.Where(m => m.Email == email && m.Password == password && m.Status == "Lock").SingleOrDefault();
            if (tk != null)
            {
                Session["user"] = tk;
                Session["Name_cus"] = tk.Name;
                Session["email"] = tk.Email;
                Session["ID_cus"] = tk.ID;
                return RedirectToAction("Home", "_clientProduct");
            }
            else if (tk1 != null)
            {
                ViewBag.error = "Tài khoản của bạn đã bị khóa!";
            }
            else
                ViewBag.error = "Email hoặc Password sai!";
            return this.Index();
        }
        public ActionResult register()
        {
            return View();
        }
        [HttpPost]
        public ActionResult register(Customer tk)
        {
            int phone = Convert.ToInt32(Request["phone"]);
            string email = Request["email"];
            string password = Request["password"];
            string gender = Request["gender"];
            string name = Request["name"];
            string add = Request["address"];
            Customer tk1 = db.Customers.Where(m => m.Email == email).SingleOrDefault();
            if (tk1 != null)
            {
                ViewBag.error = "Đã tồn tại tài khoản có Email: " + email;
            }
            else
            {
                tk.Name = name;
                tk.Gender = gender;
                tk.Address = add;
                tk.Phone_Number = phone;
                tk.Email = email;
                tk.Password = password;
                tk.Status = "Active";
                tk.created_at = DateTime.Now;
                db.Customers.InsertOnSubmit(tk);
                db.SubmitChanges();
                Session["ID_cus"] = tk.ID;
                return RedirectToAction("notification", "_clientLogin");
            }
            return View();
        }
        public ActionResult notification()
        {
            var tb = from c in db.Customers
                     where c.ID == Convert.ToInt32(Session["ID_cus"])
                     select c;
            return View(tb);
        }
        public ActionResult logout()
        {
            if (Session["user"] != null)
            {
                Session["user"] = null;
                Session["Name_cus"] = null;
                Session["ID_cus"] = null;
                return RedirectToAction("Home", "_clientProduct");
            }
            return View();
        }
    }
}