﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using thuctap.Models;
using PagedList;

namespace thuctap.Controllers
{
    public class newController : Controller
    {
        // GET: new
        bangiayDataContext db = new bangiayDataContext();
        public ActionResult Index(int? page)
        {
            if (Session["admin"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            var tin = from b in db.News
                      select b;
            ViewBag.tin = tin;
            return View(tin.ToPagedList(page ?? 1, 5));
        }
        [HttpPost]
        public ActionResult Index(int? page, FormCollection a)
        {
            string title = Request["key"];
            var tin = from b in db.News
                      where b.Title.Contains(title)
                      select b;
            return View(tin.ToPagedList(page ?? 1, 5));
        }
        public ActionResult Create()
        {
            if (Session["admin"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            var tin = (from b in db.News
                       orderby b.ID descending
                       select b).Take(3);
            return View(tin);
        }
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Create(New tin)
        {
            New test = db.News.Where(p => p.Title == Request["Title"]).FirstOrDefault();
            ViewBag.date = DateTime.Now;
            if (test != null)
            {
                ViewBag.test = "Tiêu đề " + Request["Title"] + " đã tồn tại!";
            }
            else
            {
                tin.Title = Request["Title"];
                tin.Content = Request["Content"];
                tin.Image = Request["Anh"];
                tin.created_at = ViewBag.date;
                db.News.InsertOnSubmit(tin);
                db.SubmitChanges();
            }

            return this.Create();
        }
        public ActionResult Edit(int id)
        {
            var tin = db.News.First(b => b.ID == id);
            return View(tin);
        }
        [HttpPost]
        public ActionResult Edit(int id, New tin)
        {
            /*var filename = Request["anh"];*/
            ViewBag.date = DateTime.Now;
            tin = db.News.Where(b => b.ID == id).SingleOrDefault();
            tin.Title = Request["Title"];
            tin.Content = Request["Content"];
            tin.Image = Request["Anh"];
            tin.updated_at = ViewBag.date;
            /*
            tin.Image = "/Content/tin/Image/" + GetFileName(file.FileName);*/
            UpdateModel(tin);
            db.SubmitChanges();
            return RedirectToAction("Index");

        }
        public ActionResult Edit_cre(int id)
        {
            var tin = db.News.First(b => b.ID == id);
            return View(tin);
        }
        [HttpPost]
        public ActionResult Edit_cre(int id, New tin)
        {
            /*var filename = Request["anh"];*/
            ViewBag.date = DateTime.Now;
            tin = db.News.Where(b => b.ID == id).SingleOrDefault();
            tin.Title = Request["Title"];
            tin.Content = Request["Content"];
            tin.Image = Request["Anh"];
            tin.updated_at = ViewBag.date;
            /*
            tin.Image = "/Content/tin/Image/" + GetFileName(file.FileName);*/
            UpdateModel(tin);
            db.SubmitChanges();
            return RedirectToAction("Create");

        }
        public ActionResult Delete(int id)
        {
            var tin = db.News.Where(b => b.ID == id).SingleOrDefault();
            db.News.DeleteOnSubmit(tin);
            db.SubmitChanges();
            return RedirectToAction("Index");
        }
        public ActionResult Delete_cre(int id)
        {
            var tin = db.News.Where(b => b.ID == id).SingleOrDefault();
            db.News.DeleteOnSubmit(tin);
            db.SubmitChanges();
            return RedirectToAction("Create");
        }
    }
}