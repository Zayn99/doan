﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using thuctap.Models;
namespace thuctap.Controllers
{
    public class customerController : Controller
    {
        // GET: customer
        bangiayDataContext db = new bangiayDataContext();
        // GET: customert
        public ActionResult Index()
        {
            if (Session["admin"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            var cus = from b in db.Customers
                      select b;
            return View(cus);
        }
        [HttpPost]
        public ActionResult Index(FormCollection a)
        {
            string email = Request["key"];
            var cus = from b in db.Customers
                      where b.Email.Contains(email)
                      select b;
            return View(cus);
        }
        public ActionResult Edit(int id)
        {
            var cus = db.Customers.First(b => b.ID == id);
            return View(cus);
        }
        [HttpPost]
        public ActionResult Edit(int id, Customer cus)
        {
            ViewBag.date = DateTime.Now;
            /*var filename = Request["anh"];*/
            cus = db.Customers.Where(b => b.ID == id).SingleOrDefault();
            cus.Status = Request["sta"];
            cus.note = Request["note"];
            cus.updated_at = ViewBag.date;
            UpdateModel(cus);
            db.SubmitChanges();
            return RedirectToAction("Index");
        }
        public ActionResult Delete(int id)
        {
            var cus = db.Customers.Where(b => b.ID == id).SingleOrDefault();
            db.Customers.DeleteOnSubmit(cus);
            db.SubmitChanges();
            return RedirectToAction("Index");
        }
    }
}