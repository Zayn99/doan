﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using thuctap.Models;
using PagedList;

namespace thuctap.Controllers
{
    public class productColorController : Controller
    {
        // GET: productColor
        bangiayDataContext db = new bangiayDataContext();
        public ActionResult Index()
        {
            if (Session["admin"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }
        public ActionResult Color(int? page)
        {
            if (Session["admin"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            var color = from p in db.Product_Colors
                        select p;
            ViewBag.col = color;
            ViewBag.product = from b in db.Products
                              select b;
            ViewBag.co = from b in db.Colorrs
                         select b;
            ViewBag.color = (from p in db.Product_Colors
                             orderby p.ID_Color descending
                             select p).Take(3);
            
            return View(color.ToPagedList(page ?? 1, 5));
        }
        [HttpPost]
        public ActionResult Color(int? page, Product_Color ccolor)
        {
            ViewBag.product = from b in db.Products
                              select b;
            ViewBag.co = from b in db.Colorrs
                         select b;
            var color = from p in db.Product_Colors
                        select p;
            ViewBag.col = color;
            ViewBag.color = from p in db.Product_Colors
                            orderby p.ID_Color descending
                            select p;
            Product_Color test = db.Product_Colors.Where(p => p.ID_Product == Convert.ToInt32(Request["ID_SP"])
                                                        && p.ID_Color == Convert.ToInt32(Request["Mau"])
                                                        && p.size == (Request["size"])).FirstOrDefault();
            int sl = Convert.ToInt32(Request["SL"]);
            ViewBag.date = DateTime.Now;
            if (test != null)
            {
                int a = (int) test.Quantity+sl;
                test.Quantity = a;
                UpdateModel(ccolor);
                db.SubmitChanges();
                return RedirectToAction("Color", "productColor");
            }
            else
            {
                ccolor.ID_Product = Convert.ToInt32(Request["ID_SP"]);
                ccolor.ID_Color = Convert.ToInt32(Request["Mau"]);
                ccolor.Quantity = Convert.ToInt32(Request["SL"]);
                ccolor.size = (Request["size"]);
                ccolor.created_at = ViewBag.date;
                db.Product_Colors.InsertOnSubmit(ccolor);
                db.SubmitChanges();
                return RedirectToAction("Color", "productColor");
            }

            return View(color.ToPagedList(page ?? 1, 5));
        }


        //public ActionResult Search_co(int? page)
        //{
        //    if (Session["admin"] == null)
        //    {
        //        return RedirectToAction("Index", "Home");
        //    }
        //    var color = from p in db.Product_Colors
        //                select p;
        //    ViewBag.product = from b in db.Products
        //                      select b;
        //    ViewBag.co = from b in db.Colorrs
        //                 select b;
        //    ViewBag.color = (from p in db.Product_Colors
        //                     orderby p.ID_Color descending
        //                     select p).Take(3);
        //    return View(color.ToPagedList(page ?? 1, 5));
        //}
        //[HttpPost]
        //public ActionResult Search_co(int? page, Colorr a)
        //{
        //    ViewBag.product = from b in db.Products
        //                      select b;
        //    ViewBag.co = from b in db.Colorrs
        //                 select b;
        //    ViewBag.color = (from p in db.Product_Colors
        //                     orderby p.ID_Color descending
        //                     select p).Take(3);
        //    int id = Convert.ToInt32(Request["key"]);
        //    var color = from p in db.Product_Colors
        //                where p.ID_Product == id
        //                select p;
        //    return View(color.ToPagedList(page ?? 1, 5));
        //}
        public ActionResult Delete_co(int id)
        {
            var color = db.Product_Colors.Where(b => b.ID == id).SingleOrDefault();
            db.Product_Colors.DeleteOnSubmit(color);
            db.SubmitChanges();
            return RedirectToAction("Color");
        }
    }
}