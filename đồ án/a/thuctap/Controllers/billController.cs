﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using thuctap.Models;
using PagedList;

namespace thuctap.Controllers
{
    public class billController : Controller
    {
        // GET: bill
        bangiayDataContext db = new bangiayDataContext();
        public ActionResult Index(int? page)
        {
            if (Session["admin"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            var bill = from b in db.Bills
                       orderby b.Date_order descending
                       select b;
            ViewBag.bill = bill;
            return View(bill.ToPagedList(page ?? 1, 5));
        }
        public ActionResult Bill_Detail(int id)
        {
            ViewBag.bill_detail = (from bd in db.Bill_Details
                                   join b in db.Bills on bd.ID_Bill equals b.ID
                                   join cus in db.Customers on b.ID_Customer equals cus.ID
                                   join pro_co in db.Product_Colors on bd.ID_Product_Color equals pro_co.ID
                                   join pro in db.Products on pro_co.ID_Product equals pro.ID
                                   join bra in db.Brands on pro.ID_Brand equals bra.ID
                                   join co in db.Colorrs on pro_co.ID_Color equals co.ID
                                   where bd.ID_Bill == id
                                   select new Bill_Detaill
                                   {
                                       ID = bd.ID,
                                       ID_Bill = b.ID,
                                       Quantity = (int)bd.Quantity,
                                       Cus_Name = cus.Name,
                                       Cus_Email = cus.Email,
                                       Bill_Add = b.Address,
                                       Cus_Phone = (int)b.Phone_Number,
                                       Cus_Gender = cus.Gender,
                                       Pro_Name = pro.Name,                                      
                                       Pro_Brand = bra.Name,                                      
                                       Pro_Price = (decimal)pro.Promotion_Price,
                                       Pro_Color = co.Color
                                   }).Distinct();
            ViewBag.Cus_Detail = (from bd in db.Bill_Details
                                  join b in db.Bills on bd.ID_Bill equals b.ID
                                  join cus in db.Customers on b.ID_Customer equals cus.ID
                                  /*join pro_co in db.Product_Colors on bd.ID_Product_Color equals pro_co.ID
                                  join pro in db.Products on pro_co.ID_Product equals pro.ID
                                  join bra in db.Brands on pro.ID_Brand equals bra.ID
                                  join co in db.Colorrs on pro_co.ID_Color equals co.ID*/
                                  where bd.ID_Bill == id
                                  select new Bill_Detaill
                                  {
                                      ID_Bill = b.ID,
                                      Cus_Name = cus.Name,
                                      Cus_Email = cus.Email,
                                      Cus_Phone = (int)b.Phone_Number,
                                      Bill_Add = b.Address,

                                  }).Distinct();
            return View();
        }
    }
}