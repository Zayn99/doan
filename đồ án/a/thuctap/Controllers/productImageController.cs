﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using thuctap.Models;
using PagedList;
namespace thuctap.Controllers
{
    public class productImageController : Controller
    {
        // GET: productImage
        bangiayDataContext db = new bangiayDataContext();
        public ActionResult Index()
        {
            if (Session["admin"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }
        public ActionResult Image(int? page)
        {
            if (Session["admin"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            var image = from p in db.Imagees
                        select p;
            ViewBag.i = image;
            ViewBag.product = from b in db.Products
                              select b;
            ViewBag.image = (from p in db.Imagees
                             orderby p.ID descending
                             select p).Take(3);
            return View(image.ToPagedList(page ?? 1, 5));
        }
        [HttpPost]
        public ActionResult Image(int? page, Imagee iimgae)
        {

            var image = from p in db.Imagees
                        select p;
            ViewBag.product = from b in db.Products
                              select b;
            ViewBag.i = image;
            ViewBag.image = (from p in db.Imagees
                             orderby p.ID descending
                             select p).Take(3);
            Product test = db.Products.Where(p => p.ID == Convert.ToInt32(Request["ID_SP"])).FirstOrDefault();
            ViewBag.date = DateTime.Now;
            if (test == null)
            {
                ViewBag.test = "Sản phẩm có ID " + Request["ID_SP"] + " không tồn tại!";
            }
            else
            {
                iimgae.ID_Product = Convert.ToInt32(Request["ID_SP"]);
                iimgae.Image = Request["Anh"];
                iimgae.created_at = ViewBag.date;
                db.Imagees.InsertOnSubmit(iimgae);
                db.SubmitChanges();
            }
            return View(image.ToPagedList(page ?? 1, 5));
        }
        //public ActionResult Search(int? page)
        //{
        //    if (Session["admin"] == null)
        //    {
        //        return RedirectToAction("Index", "Home");
        //    }
        //    var image = from p in db.Imagees
        //                select p;
        //    ViewBag.product = from b in db.Products
        //                      select b;
        //    ViewBag.image = (from p in db.Imagees
        //                     orderby p.ID descending
        //                     select p).Take(3);
        //    return View(image.ToPagedList(page ?? 1, 5));
        //}
        //[HttpPost]
        //public ActionResult Search(int? page, Imagee a)
        //{
        //    int id = Convert.ToInt32(Request["key"]);
        //    /*var image = from p in db.Imagees
        //                select p;*/
        //    ViewBag.product = from b in db.Products
        //                      select b;
        //    ViewBag.image = (from p in db.Imagees
        //                     orderby p.ID descending
        //                     select p).Take(3);
        //    var image = from p in db.Imagees
        //                where p.ID_Product == id
        //                select p;
        //    return View(image.ToPagedList(page ?? 1, 5));
        //}

        public ActionResult Delete_img(int id)
        {
            var img = db.Imagees.Where(b => b.ID == id).SingleOrDefault();
            db.Imagees.DeleteOnSubmit(img);
            db.SubmitChanges();
            return RedirectToAction("Image");
        }
    }
}