﻿using System;
using System.Linq;
using System.Web.Mvc;
using thuctap.Models;
using PagedList;
namespace thuctap.Controllers

{
    public class _clientProductDetailControllerController : Controller
    {
        bangiayDataContext db = new bangiayDataContext();
        // GET: _clientProductDetailController
        public ActionResult Index()
        {
            int key = Convert.ToInt32(Request["key"]);
            Session["thangghe"] = key;
            ViewBag.ctsp = (from b in db.Products
                            where b.ID == key
                            orderby b.ID descending
                            select b).Take(4);
            ViewBag.image = (from b in db.Imagees
                             where b.ID_Product == key
                             select new SoLuong
                             {
                                 Image = b.Image
                             }
                            ); ;
            ViewBag.ID_pro = (from p in db.Products
                              where p.ID == key
                              select new SoLuong
                              {
                                  ID_Product_Color = (int)p.ID,
                              });
            ViewBag.color = from p in db.Products
                            join pro_co in db.Product_Colors on p.ID equals (pro_co.ID_Product)
                            join co in db.Colorrs on pro_co.ID_Color equals (co.ID)
                            where p.ID == key 
                            select new SoLuong
                            {
                                Quantity = (int)pro_co.Quantity,
                                Color = co.Color,
                                ID_Product_Color = (int)pro_co.ID,
                                Image = co.Image,
                                Size = pro_co.size,
                            };
            ViewBag.count = (from p in db.Products
                             join pro_co in db.Product_Colors on p.ID equals (pro_co.ID_Product)
                             join co in db.Colorrs on pro_co.ID_Color equals (co.ID)
                             where p.ID == key
                             select new SoLuong
                             {
                                 Quantity = (int) pro_co.Quantity,
                                 Color = co.Color,
                                 Image = co.Image
                             }).Count();

            return View();
        }

        public ActionResult locmau(string sortOrder)
        {
            int key = Convert.ToInt32(Session["thangghe"]);
            /*var color = (from p in db.Products
                            join pro_co in db.Product_Colors on p.ID equals (pro_co.ID_Product)
                            join co in db.Colorrs on pro_co.ID_Color equals (co.ID)
                            where p.ID == key && pro_co.size == "39"
                         select new SoLuong
                            {
                                Quantity = (int)pro_co.Quantity,
                                Color = co.Color,
                                ID_Product_Color = (int)pro_co.ID,
                                Image = co.Image,
                                Size = pro_co.size,
                            }).ToList();*/
            switch (sortOrder)
            {
                
                case "a":
                    ViewBag.color = null;
                    break;
                case "b":
                    ViewBag.color = (from p in db.Products
                                    join pro_co in db.Product_Colors on p.ID equals (pro_co.ID_Product)
                                    join co in db.Colorrs on pro_co.ID_Color equals (co.ID)
                                    where  p.ID == key && pro_co.size == "39"
                                    select new SoLuong
                                    {
                                        ID=pro_co.ID,
                                        Quantity = (int)pro_co.Quantity,
                                        Color = co.Color,
                                        ID_Product_Color = (int)pro_co.ID,
                                        Image = co.Image,
                                        Size = pro_co.size,
                                    }).ToList();
                    break;
                case "c":
                    ViewBag.color = (from p in db.Products
                                     join pro_co in db.Product_Colors on p.ID equals (pro_co.ID_Product)
                                     join co in db.Colorrs on pro_co.ID_Color equals (co.ID)
                                     where p.ID == key && pro_co.size == "40"
                                     select new SoLuong
                                     {
                                         ID = pro_co.ID,
                                         Quantity = (int)pro_co.Quantity,
                                         Color = co.Color,
                                         ID_Product_Color = (int)pro_co.ID,
                                         Image = co.Image,
                                         Size = pro_co.size,
                                     }).ToList();
                    break;
                case "d":
                    ViewBag.color = (from p in db.Products
                                     join pro_co in db.Product_Colors on p.ID equals (pro_co.ID_Product)
                                     join co in db.Colorrs on pro_co.ID_Color equals (co.ID)
                                     where p.ID == key && pro_co.size == "41"
                                     select new SoLuong
                                     {
                                         ID = pro_co.ID,
                                         Quantity = (int)pro_co.Quantity,
                                         Color = co.Color,
                                         ID_Product_Color = (int)pro_co.ID,
                                         Image = co.Image,
                                         Size = pro_co.size,
                                     }).ToList();
                    break;
                case "e":
                    ViewBag.color = null;
                    break;
                default:
                    break;
            }
            return View(/*color.ToPagedList(1,5)*/);
        }

        public ActionResult brand(int? page)
        {

            string bran = Request["brand"];
            Session["brand"]=bran;
            ViewBag.tb = "Sản phẩm hãng " + bran;
            var brand = (from b in db.Products
                        join br in db.Brands on b.ID_Brand equals (br.ID)
                        where br.Name == bran
                        select b).ToList();
            ViewBag.count = (from b in db.Products
                             join br in db.Brands on b.ID_Brand equals (br.ID)
                             where br.Name == bran
                             select b).Count();
            ViewBag.brand = brand;
            return View(brand.ToPagedList(page ?? 1, 4));
        }
        [HttpPost]
        public ActionResult product(int? page, FormCollection data)
        {

            string key = Request["key"];
            ViewBag.TK = "Có ";
            ViewBag.TK1 = " sản phẩm trùng với: " + key;
            var product = from b in db.Products
                          where b.Name.Contains(key)
                          select b;
            ViewBag.count = (from b in db.Products
                             where b.Name.Contains(key)
                             select b).Count();

            return View(product.ToPagedList(page ?? 1, 4));
        }
    }
}