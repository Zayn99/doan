USE [shoes]
GO
/****** Object:  Table [dbo].[Admin]    Script Date: 4/8/2021 7:47:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Admin](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Email] [varchar](60) NULL,
	[Password] [varchar](15) NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
	[Name] [nvarchar](30) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Bill_Detail]    Script Date: 4/8/2021 7:47:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bill_Detail](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ID_Bill] [int] NULL,
	[ID_Product_Color] [int] NULL,
	[Quantity] [int] NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Bills]    Script Date: 4/8/2021 7:47:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bills](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ID_Customer] [int] NULL,
	[Date_order] [datetime] NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
	[Address] [nvarchar](100) NULL,
	[Phone_Number] [int] NULL,
	[Confirm] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Brand]    Script Date: 4/8/2021 7:47:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Brand](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](30) NULL,
	[Description] [nvarchar](300) NULL,
	[Image] [nvarchar](200) NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cart]    Script Date: 4/8/2021 7:47:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cart](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ID_Product_Color] [int] NULL,
	[ID_Customer] [int] NULL,
	[Quantity_Purchased] [int] NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Colorr]    Script Date: 4/8/2021 7:47:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Colorr](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Color] [nvarchar](20) NULL,
	[Image] [nvarchar](200) NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 4/8/2021 7:47:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Email] [varchar](50) NOT NULL,
	[Password] [varchar](15) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[Gender] [nvarchar](20) NULL,
	[Address] [nvarchar](200) NULL,
	[Phone_Number] [int] NULL,
	[note] [nvarchar](200) NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
	[Status] [char](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Favorites_list]    Script Date: 4/8/2021 7:47:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Favorites_list](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ID_Product] [int] NULL,
	[ID_Customer] [int] NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Imagee]    Script Date: 4/8/2021 7:47:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Imagee](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ID_Product] [int] NULL,
	[Image] [nvarchar](200) NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[New_Images]    Script Date: 4/8/2021 7:47:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[New_Images](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ID_New] [int] NULL,
	[Image] [nvarchar](300) NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[News]    Script Date: 4/8/2021 7:47:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[News](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](100) NULL,
	[Content] [ntext] NULL,
	[Image] [nvarchar](200) NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Product]    Script Date: 4/8/2021 7:47:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[ID_Brand] [int] NULL,
	[Description] [nvarchar](300) NULL,
	[Price] [money] NULL,
	[Promotion_Price] [money] NULL,
	[Image] [nvarchar](200) NULL,
	[Warranty] [nvarchar](50) NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Product_Color]    Script Date: 4/8/2021 7:47:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product_Color](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ID_Color] [int] NULL,
	[ID_Product] [int] NULL,
	[Quantity] [int] NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
	[size] [char](2) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Slide]    Script Date: 4/8/2021 7:47:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Slide](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Image] [nvarchar](100) NULL,
	[Title] [nvarchar](100) NULL,
	[Content] [nvarchar](500) NULL,
	[Action] [nvarchar](30) NULL,
	[Discount] [int] NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Admin] ON 

INSERT [dbo].[Admin] ([ID], [Email], [Password], [created_at], [updated_at], [Name]) VALUES (1, N'zaynmilak99@gmail.com', N'123', NULL, NULL, N'Zayn')
SET IDENTITY_INSERT [dbo].[Admin] OFF
GO
SET IDENTITY_INSERT [dbo].[Bill_Detail] ON 

INSERT [dbo].[Bill_Detail] ([ID], [ID_Bill], [ID_Product_Color], [Quantity], [created_at], [updated_at]) VALUES (1, 1, 68, 2, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Bill_Detail] OFF
GO
SET IDENTITY_INSERT [dbo].[Bills] ON 

INSERT [dbo].[Bills] ([ID], [ID_Customer], [Date_order], [created_at], [updated_at], [Address], [Phone_Number], [Confirm]) VALUES (1, 1, CAST(N'2021-04-05T08:20:21.543' AS DateTime), NULL, NULL, N'Nhà số 9, ngách 99/48 Phường Phúc Diễn Quận Bắc Từ Liêm Hà Nội', 852049203, N'Chờ xác nhận')
SET IDENTITY_INSERT [dbo].[Bills] OFF
GO
SET IDENTITY_INSERT [dbo].[Brand] ON 

INSERT [dbo].[Brand] ([ID], [Name], [Description], [Image], [created_at], [updated_at]) VALUES (1, N'NIKE', N'Chất lượng Rep 1:1 ', N'https://bizweb.dktcdn.net/100/336/177/products/9-5404bf6a-5b82-4dd4-bb96-40fe73cc8ad0.jpg?v=1616478847267', CAST(N'2021-04-01T08:42:00.840' AS DateTime), NULL)
INSERT [dbo].[Brand] ([ID], [Name], [Description], [Image], [created_at], [updated_at]) VALUES (2, N'ADIDAS', N'Chất lượng Rep 1:1 ', N'https://bizweb.dktcdn.net/100/336/177/products/50003341-2365870303644467-7038016969261449216-o.jpg?v=1547535181000', CAST(N'2021-04-01T08:43:19.477' AS DateTime), NULL)
INSERT [dbo].[Brand] ([ID], [Name], [Description], [Image], [created_at], [updated_at]) VALUES (3, N'BALENCIAGA', N'Chất lượng Rep 1:1 ', N'https://giaygiare.vn/upload/sanpham/thumbs/balenciaga-track-triple-white-nam-nu-1-1.jpg', CAST(N'2021-04-01T08:47:47.027' AS DateTime), CAST(N'2021-04-01T08:48:57.130' AS DateTime))
INSERT [dbo].[Brand] ([ID], [Name], [Description], [Image], [created_at], [updated_at]) VALUES (4, N'GUCCI', N'Chất lượng Rep 1:1 ', N'https://bizweb.dktcdn.net/100/336/177/products/dscf2988.jpg?v=1563162475013', CAST(N'2021-04-01T08:49:42.263' AS DateTime), NULL)
INSERT [dbo].[Brand] ([ID], [Name], [Description], [Image], [created_at], [updated_at]) VALUES (5, N'VANS', N'Chất lượng Rep 1:1 ', N'https://bizweb.dktcdn.net/100/336/177/products/88-40092ec2-9c6a-4867-8a70-ba056fd33d8f.jpg?v=1547967316000', CAST(N'2021-04-01T08:50:20.350' AS DateTime), NULL)
INSERT [dbo].[Brand] ([ID], [Name], [Description], [Image], [created_at], [updated_at]) VALUES (6, N'CONVERSE', N'Chất lượng Rep 1:1 ', N'https://bizweb.dktcdn.net/100/336/177/products/17.jpg?v=1546919922130', CAST(N'2021-04-01T08:51:07.650' AS DateTime), NULL)
INSERT [dbo].[Brand] ([ID], [Name], [Description], [Image], [created_at], [updated_at]) VALUES (7, N'MLB', N'Chất lượng Rep 1:1 ', N'https://bizweb.dktcdn.net/100/336/177/products/dscf4560.jpg?v=1573540617000', CAST(N'2021-04-01T09:42:51.780' AS DateTime), CAST(N'2021-04-01T09:44:46.960' AS DateTime))
INSERT [dbo].[Brand] ([ID], [Name], [Description], [Image], [created_at], [updated_at]) VALUES (10, N'NEW BALANCE', N'Chất lượng Rep 1:1 ', N'https://bizweb.dktcdn.net/100/336/177/products/dscf6661.jpg?v=1592988269917', CAST(N'2021-04-01T09:43:45.770' AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[Brand] OFF
GO
SET IDENTITY_INSERT [dbo].[Cart] ON 

INSERT [dbo].[Cart] ([ID], [ID_Product_Color], [ID_Customer], [Quantity_Purchased], [created_at], [updated_at]) VALUES (11, 204, 1, 1, CAST(N'2021-04-05T16:08:43.977' AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[Cart] OFF
GO
SET IDENTITY_INSERT [dbo].[Colorr] ON 

INSERT [dbo].[Colorr] ([ID], [Color], [Image], [created_at], [updated_at]) VALUES (1, N'Đen', N'https://bizweb.dktcdn.net/100/336/177/products/10-81ed6bc2-ac8a-4b0a-8746-7ca4de21a0ab.jpg?v=1616478878337', CAST(N'2021-04-02T07:48:06.530' AS DateTime), CAST(N'2021-04-02T07:48:24.220' AS DateTime))
INSERT [dbo].[Colorr] ([ID], [Color], [Image], [created_at], [updated_at]) VALUES (2, N'Trắng', N'https://bizweb.dktcdn.net/100/336/177/products/16-7101df84-9f82-4179-830d-67993c5ca4a1.jpg?v=1616478946000', CAST(N'2021-04-02T07:50:01.697' AS DateTime), NULL)
INSERT [dbo].[Colorr] ([ID], [Color], [Image], [created_at], [updated_at]) VALUES (3, N'Đỏ', N'https://bizweb.dktcdn.net/100/336/177/products/27-3e2e1f85-449a-46f8-85ea-72ddd361145e.jpg?v=1611559414000', CAST(N'2021-04-02T07:50:17.477' AS DateTime), NULL)
INSERT [dbo].[Colorr] ([ID], [Color], [Image], [created_at], [updated_at]) VALUES (4, N'Vàng', N'https://bizweb.dktcdn.net/100/336/177/products/dscf6243.jpg?v=1590302139563', CAST(N'2021-04-02T07:50:41.070' AS DateTime), NULL)
INSERT [dbo].[Colorr] ([ID], [Color], [Image], [created_at], [updated_at]) VALUES (5, N'Xanh', N'https://bizweb.dktcdn.net/100/336/177/products/64.jpg?v=1606282824817', CAST(N'2021-04-02T07:51:13.843' AS DateTime), NULL)
INSERT [dbo].[Colorr] ([ID], [Color], [Image], [created_at], [updated_at]) VALUES (6, N'Xám', N'https://bizweb.dktcdn.net/100/336/177/products/60.jpg?v=1606282851000', CAST(N'2021-04-02T07:51:38.303' AS DateTime), NULL)
INSERT [dbo].[Colorr] ([ID], [Color], [Image], [created_at], [updated_at]) VALUES (7, N'Cam', N'https://bizweb.dktcdn.net/100/336/177/products/2-1.jpg?v=1598953701000', CAST(N'2021-04-02T07:52:07.137' AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[Colorr] OFF
GO
SET IDENTITY_INSERT [dbo].[Customer] ON 

INSERT [dbo].[Customer] ([ID], [Email], [Password], [Name], [Gender], [Address], [Phone_Number], [note], [created_at], [updated_at], [Status]) VALUES (1, N'zaynmilak99@gmail.com', N'1234', N'Thắng Nguyễn', N'Nam', N'Nhà số 9, ngách 99/48 Phường Phúc Diễn Quận Bắc Từ Liêm Hà Nội', 852049203, NULL, CAST(N'2021-04-05T08:19:26.780' AS DateTime), CAST(N'2021-04-05T16:31:09.603' AS DateTime), N'Active              ')
SET IDENTITY_INSERT [dbo].[Customer] OFF
GO
SET IDENTITY_INSERT [dbo].[Favorites_list] ON 

INSERT [dbo].[Favorites_list] ([ID], [ID_Product], [ID_Customer], [created_at], [updated_at]) VALUES (5, 1, 1, CAST(N'2021-04-08T07:40:52.120' AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[Favorites_list] OFF
GO
SET IDENTITY_INSERT [dbo].[Imagee] ON 

INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (7, 1, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/6-dad445b8-9d65-4381-a564-02429370df11.jpg?v=1616832056713', CAST(N'2021-04-02T08:25:39.747' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (8, 1, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/7-38850fad-629e-426f-a2ac-b6c7a607542c.jpg?v=1616832057260', CAST(N'2021-04-02T08:25:50.600' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (9, 1, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/8-14ace31a-a33f-4f3d-ac03-53ed008fee4e.jpg?v=1616832057857', CAST(N'2021-04-02T08:25:57.927' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (10, 1, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/9-85b97316-abee-4199-b47a-0b6ef247d45f.jpg?v=1616832058190', CAST(N'2021-04-02T08:26:04.863' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (11, 8, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/1-e9ca1c74-a64a-4822-aa6d-063d01888bb1.jpg?v=1616479119313', CAST(N'2021-04-05T11:08:05.637' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (12, 8, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/1-e9ca1c74-a64a-4822-aa6d-063d01888bb1.jpg?v=1616479119313', CAST(N'2021-04-05T11:08:12.630' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (13, 8, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/3-86e378e2-782f-4394-8362-c99b52502c69.jpg?v=1616479120120', CAST(N'2021-04-05T11:08:19.620' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (14, 8, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/4-511ef46f-1817-443e-b5e9-8cb573f37651.jpg?v=1616479120450', CAST(N'2021-04-05T11:08:28.280' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (15, 10, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/11-4ffeedd5-50b1-40aa-a2ae-7bdc259bb38d.jpg?v=1616478916343', CAST(N'2021-04-05T11:09:37.697' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (16, 10, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/12-b486e5d2-7d09-4947-838e-62d428bb6716.jpg?v=1616478916603', CAST(N'2021-04-05T11:09:45.710' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (17, 10, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/13-7aedd029-69af-4c5d-aadc-cd2a2c047f5b.jpg?v=1616478917127', CAST(N'2021-04-05T11:09:53.270' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (18, 10, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/14-d5ba0472-3c9f-4017-8ce8-7ffa8cda3128.jpg?v=1616478917437', CAST(N'2021-04-05T11:10:01.960' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (19, 9, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/23-4a4313ba-8550-4b43-9a53-7721960690ba.jpg?v=1616479015763', CAST(N'2021-04-05T11:10:54.797' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (20, 9, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/23-4a4313ba-8550-4b43-9a53-7721960690ba.jpg?v=1616479015763', CAST(N'2021-04-05T11:10:59.137' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (21, 9, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/25-7e2f9589-4800-4fe6-a3a4-d07bbbddf023.jpg?v=1616479016613', CAST(N'2021-04-05T11:11:05.570' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (22, 9, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/26-142d6f9c-082d-403e-a7b9-eaa9a25ddc9b.jpg?v=1616479017093', CAST(N'2021-04-05T11:11:12.543' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (23, 11, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/26-638e34cd-4d70-44a9-a1c5-de52e2310a94.jpg?v=1611559414207', CAST(N'2021-04-05T11:11:59.327' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (24, 11, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/26-638e34cd-4d70-44a9-a1c5-de52e2310a94.jpg?v=1611559414207', CAST(N'2021-04-05T11:12:02.643' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (25, 11, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/28-ce305c50-ca15-468a-a27e-a44ac4547da5.jpg?v=1611559415193', CAST(N'2021-04-05T11:12:09.450' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (26, 11, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/29-e7e9c4df-6430-4777-b47d-cdda018abecf.jpg?v=1611559415500', CAST(N'2021-04-05T11:12:17.717' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (27, 14, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/22-e3c92372-829c-4377-9991-d5c752e8eef1.jpg?v=1611559593950', CAST(N'2021-04-05T13:25:01.290' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (28, 14, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/22-e3c92372-829c-4377-9991-d5c752e8eef1.jpg?v=1611559593950', CAST(N'2021-04-05T13:25:07.840' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (29, 14, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/24-44109e3d-b456-413c-a8d3-abf40e6ec56c.jpg?v=1611559594790', CAST(N'2021-04-05T13:25:15.143' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (30, 14, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/25-439a1b8b-26d2-4d97-9d18-ed69a3bf9381.jpg?v=1611559595270', CAST(N'2021-04-05T13:25:23.520' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (31, 15, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/1-4bea8a57-bced-4f79-9a6b-1e436ffdf979.jpg?v=1610525298593', CAST(N'2021-04-05T13:25:57.673' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (32, 15, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/1-4bea8a57-bced-4f79-9a6b-1e436ffdf979.jpg?v=1610525298593', CAST(N'2021-04-05T13:26:01.807' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (33, 15, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/3-66920506-bc0d-4f1a-8812-d31a733bbd80.jpg?v=1610525299563', CAST(N'2021-04-05T13:26:08.407' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (34, 15, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/4-ff132ae1-1958-462a-8894-78f092cdd2ea.jpg?v=1610525299830', CAST(N'2021-04-05T13:26:14.813' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (35, 17, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/67.jpg?v=1606282794180', CAST(N'2021-04-05T13:26:49.577' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (36, 17, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/68.jpg?v=1606282794633', CAST(N'2021-04-05T13:26:58.257' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (37, 17, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/69.jpg?v=1606282794887', CAST(N'2021-04-05T13:27:09.007' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (38, 17, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/70.jpg?v=1606282795127', CAST(N'2021-04-05T13:27:15.580' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (39, 18, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/31.jpg?v=1605765076400', CAST(N'2021-04-05T13:27:36.363' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (40, 18, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/32.jpg?v=1605765077423', CAST(N'2021-04-05T13:27:43.953' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (41, 18, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/33.jpg?v=1605765078297', CAST(N'2021-04-05T13:27:50.533' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (42, 18, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/34-36ad6bf9-8ef9-4220-bb20-83d30204a7f8.jpg?v=1605765079143', CAST(N'2021-04-05T13:27:57.213' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (43, 19, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6599.jpg?v=1592394663710', CAST(N'2021-04-05T13:28:37.253' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (44, 19, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6600.jpg?v=1592394665917', CAST(N'2021-04-05T13:28:47.290' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (45, 19, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6601.jpg?v=1592394668177', CAST(N'2021-04-05T13:28:53.943' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (46, 19, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6602.jpg?v=1592394670390', CAST(N'2021-04-05T13:29:02.673' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (47, 20, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf2920.jpg?v=1562751354087', CAST(N'2021-04-05T13:29:19.893' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (48, 20, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf2921.jpg?v=1562751355207', CAST(N'2021-04-05T13:29:29.080' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (49, 20, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf2922.jpg?v=1562751356133', CAST(N'2021-04-05T13:29:35.540' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (50, 20, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf2924.jpg?v=1562751357437', CAST(N'2021-04-05T13:29:42.773' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (51, 21, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/11-9d92fa80-dc2c-4ebd-a61f-378c03d8aa4b.jpg?v=1546919912917', CAST(N'2021-04-05T13:33:28.903' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (52, 21, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/12-80466ec9-118e-4db8-8596-8ac5c12c09c2.jpg?v=1546919912917', CAST(N'2021-04-05T13:33:35.123' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (53, 21, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/13-132303fd-897b-4df3-9482-62b7f7e294bd.jpg?v=1546919912917', CAST(N'2021-04-05T13:33:42.287' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (54, 21, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/14-c8679944-8361-4837-b41a-cd3cc8c2018e.jpg?v=1546919912917', CAST(N'2021-04-05T13:33:49.210' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (55, 22, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/6.jpg?v=1586864907913', CAST(N'2021-04-05T13:34:04.213' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (56, 22, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/6.jpg?v=1586864907913', CAST(N'2021-04-05T13:34:07.250' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (57, 22, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/8.jpg?v=1586864907913', CAST(N'2021-04-05T13:34:14.590' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (58, 22, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/9.jpg?v=1586864907913', CAST(N'2021-04-05T13:34:21.623' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (59, 23, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/1-63cca0a8-76b0-4d2e-9d5c-e07322672796.jpg?v=1611560048407', CAST(N'2021-04-05T13:34:39.147' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (60, 23, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/2-831fcf72-a027-4602-b7a7-072af82c2dd4.jpg?v=1611560048870', CAST(N'2021-04-05T13:34:56.093' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (61, 23, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/3-e2f515e0-7946-4e80-98ed-6e0ff7a58124.jpg?v=1611560049373', CAST(N'2021-04-05T13:35:03.577' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (62, 23, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/4-fc004c34-f5cb-4be5-8cfc-65a1d8ffcec8.jpg?v=1611560049840', CAST(N'2021-04-05T13:35:10.057' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (63, 25, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/1-1-615a8554-2631-42ff-a271-d3fba83a9240.jpg?v=1610525331493', CAST(N'2021-04-05T13:35:41.937' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (64, 25, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/2-1-5d469925-98d1-4cb7-82e8-51e7dbdad278.jpg?v=1610525332037', CAST(N'2021-04-05T13:35:49.813' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (65, 25, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/7-3355edfb-21d2-49c9-94dd-a1ecd32e14c5.jpg?v=1610525332373', CAST(N'2021-04-05T13:35:57.420' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (66, 25, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/8-fcd6899f-496e-49bb-ac54-047d06af7edf.jpg?v=1610525332670', CAST(N'2021-04-05T13:36:04.920' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (67, 26, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/1-825a5df8-3992-40e4-9a4c-69889146246c.jpg?v=1602405504007', CAST(N'2021-04-05T13:36:28.847' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (68, 26, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/2-48d0d16d-e578-47ce-85cc-a7be727ee021.jpg?v=1602405505600', CAST(N'2021-04-05T13:36:36.733' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (69, 26, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/3-7653037a-dcec-4365-b989-9c66e1b3d2ce.jpg?v=1602405506433', CAST(N'2021-04-05T13:36:45.990' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (70, 26, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/4-78a09c8f-6da9-4353-967b-eebacdc1a7d2.jpg?v=1602405507273', CAST(N'2021-04-05T13:36:53.233' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (71, 27, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9711.jpg?v=1604392718100', CAST(N'2021-04-05T13:37:13.243' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (72, 27, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9712.jpg?v=1604392720600', CAST(N'2021-04-05T13:37:20.843' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (73, 27, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9713.jpg?v=1604392723253', CAST(N'2021-04-05T13:37:27.243' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (74, 27, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9714.jpg?v=1604392726013', CAST(N'2021-04-05T13:37:34.067' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (75, 28, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6693.jpg?v=1592988685157', CAST(N'2021-04-05T13:39:22.513' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (76, 28, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6694.jpg?v=1592988687157', CAST(N'2021-04-05T13:39:29.720' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (77, 28, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6695.jpg?v=1592988689030', CAST(N'2021-04-05T13:39:36.797' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (78, 28, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6696.jpg?v=1592988690763', CAST(N'2021-04-05T13:39:43.340' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (79, 29, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6605.jpg?v=1592394850703', CAST(N'2021-04-05T13:39:59.307' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (80, 29, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6606.jpg?v=1592394853213', CAST(N'2021-04-05T13:40:08.053' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (81, 29, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6607.jpg?v=1592394855880', CAST(N'2021-04-05T13:40:15.470' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (82, 29, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6609.jpg?v=1592394858193', CAST(N'2021-04-05T13:40:21.393' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (83, 30, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/15-48373202-78de-482e-bbe9-d0aae6ca215d.jpg?v=1586864978403', CAST(N'2021-04-05T13:40:43.660' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (84, 30, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/16-6f1033e3-1838-40d7-9e86-04a3c0038498.jpg?v=1586864978850', CAST(N'2021-04-05T13:40:52.273' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (85, 30, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/17-9aa6c862-a847-4dc0-b3df-41f152854b37.jpg?v=1586864979467', CAST(N'2021-04-05T13:40:59.443' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (86, 31, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf3579.jpg?v=1565424144283', CAST(N'2021-04-05T13:41:14.823' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (87, 31, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf3580.jpg?v=1565424145477', CAST(N'2021-04-05T13:41:22.883' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (88, 31, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf3581.jpg?v=1565424146603', CAST(N'2021-04-05T13:41:31.433' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (89, 31, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf3582.jpg?v=1565424147820', CAST(N'2021-04-05T13:41:38.077' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (90, 32, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf3095.jpg?v=1562840806277', CAST(N'2021-04-05T13:41:55.360' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (91, 32, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf3096.jpg?v=1562840806277', CAST(N'2021-04-05T13:42:04.933' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (92, 32, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf3097.jpg?v=1562840806277', CAST(N'2021-04-05T13:42:11.953' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (93, 32, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf3098.jpg?v=1562840806277', CAST(N'2021-04-05T13:42:18.043' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (94, 33, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/86.jpg?v=1547837362710', CAST(N'2021-04-05T13:42:31.513' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (95, 33, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/87.jpg?v=1547837364243', CAST(N'2021-04-05T13:42:41.990' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (96, 33, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/88.jpg?v=1547837365227', CAST(N'2021-04-05T13:42:49.907' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (97, 33, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/89.jpg?v=1547837366060', CAST(N'2021-04-05T13:42:57.863' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (98, 34, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/800502-01-1.jpg?v=1546919919267', CAST(N'2021-04-05T13:44:02.333' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (99, 34, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/800502-02.jpg?v=1546919919267', CAST(N'2021-04-05T13:44:10.177' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (100, 34, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/800502-03.jpg?v=1546919919267', CAST(N'2021-04-05T13:44:15.980' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (101, 34, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/800502-04.jpg?v=1546919919267', CAST(N'2021-04-05T13:44:23.257' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (102, 35, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/49422709-2365870980311066-7436626140904554496-o.jpg?v=1547535181723', CAST(N'2021-04-05T13:44:57.653' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (103, 35, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/50003341-2365870303644467-7038016969261449216-o.jpg?v=1547535181723', CAST(N'2021-04-05T13:45:03.873' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (104, 35, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/50104204-2365870356977795-2438374537348251648-o.jpg?v=1547535181723', CAST(N'2021-04-05T13:45:10.427' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (105, 35, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/49949082-2365870383644459-5185166226636144640-o.jpg?v=1547535181723', CAST(N'2021-04-05T13:45:16.847' AS DateTime), NULL)
GO
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (106, 36, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/21-966fb1dd-a0c4-4d19-8016-31d3c13375cf.jpg?v=1547361689080', CAST(N'2021-04-05T13:45:36.297' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (107, 36, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/22-0794bee4-5e0e-4a3e-a27f-89e14503e197.jpg?v=1547361689080', CAST(N'2021-04-05T13:45:44.100' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (108, 36, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/23-eb3265dd-2279-4dd0-bfcd-741fac45eced.jpg?v=1547361689080', CAST(N'2021-04-05T13:45:53.143' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (109, 36, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/24-b4eb706a-ebd4-4296-84ee-0c92b27b35c0.jpg?v=1547361689080', CAST(N'2021-04-05T13:45:59.463' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (110, 48, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6721.jpg?v=1592988113943', CAST(N'2021-04-05T13:46:42.157' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (111, 48, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6722.jpg?v=1592988116000', CAST(N'2021-04-05T13:46:51.423' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (112, 48, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6723.jpg?v=1592988117587', CAST(N'2021-04-05T13:46:59.413' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (113, 48, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6724.jpg?v=1592988119493', CAST(N'2021-04-05T13:47:10.053' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (114, 49, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf2974.jpg?v=1562751235627', CAST(N'2021-04-05T13:47:25.460' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (115, 49, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf2975.jpg?v=1562751236723', CAST(N'2021-04-05T13:47:32.740' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (116, 49, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf2976.jpg?v=1562751237783', CAST(N'2021-04-05T13:47:40.423' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (117, 49, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf2977.jpg?v=1562751238720', CAST(N'2021-04-05T13:47:48.003' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (118, 50, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf2963.jpg?v=1562751312203', CAST(N'2021-04-05T13:48:06.697' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (119, 50, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf2964.jpg?v=1562751313383', CAST(N'2021-04-05T13:48:15.067' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (120, 50, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf2965.jpg?v=1562751314213', CAST(N'2021-04-05T13:48:24.797' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (121, 50, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf2966.jpg?v=1562751315057', CAST(N'2021-04-05T13:48:34.107' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (122, 51, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf2986.jpg?v=1563162475013', CAST(N'2021-04-05T13:48:53.550' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (123, 51, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf2988.jpg?v=1563162475013', CAST(N'2021-04-05T13:49:02.547' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (124, 51, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf2989.jpg?v=1563162475013', CAST(N'2021-04-05T13:49:14.170' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (125, 51, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf2990.jpg?v=1563162475013', CAST(N'2021-04-05T13:49:22.433' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (126, 52, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf2980.jpg?v=1563162535740', CAST(N'2021-04-05T13:50:25.650' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (127, 52, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf2981.jpg?v=1563162535740', CAST(N'2021-04-05T13:50:39.320' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (128, 52, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf2982.jpg?v=1563162535740', CAST(N'2021-04-05T13:50:46.633' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (129, 52, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf2983.jpg?v=1563162535740', CAST(N'2021-04-05T13:50:54.580' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (130, 53, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf4670.jpg?v=1575438868097', CAST(N'2021-04-05T13:51:15.183' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (131, 53, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf4671.jpg?v=1575438868097', CAST(N'2021-04-05T13:51:22.743' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (132, 53, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf4672.jpg?v=1575438868097', CAST(N'2021-04-05T13:51:28.837' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (133, 53, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf4673.jpg?v=1575438868097', CAST(N'2021-04-05T13:51:36.517' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (134, 54, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9056.jpg?v=1598952561870', CAST(N'2021-04-05T13:52:09.547' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (135, 54, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9057.jpg?v=1598952563603', CAST(N'2021-04-05T13:52:21.233' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (136, 54, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9058.jpg?v=1598952565263', CAST(N'2021-04-05T13:52:29.643' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (137, 54, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9059.jpg?v=1598952567220', CAST(N'2021-04-05T13:52:43.753' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (138, 55, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9367.jpg?v=1602243560420', CAST(N'2021-04-05T13:53:08.920' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (139, 55, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9368.jpg?v=1602243562447', CAST(N'2021-04-05T13:53:30.363' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (140, 55, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9369.jpg?v=1602243564577', CAST(N'2021-04-05T13:53:39.753' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (141, 55, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9370.jpg?v=1602243566353', CAST(N'2021-04-05T13:53:47.470' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (142, 56, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf3849.jpg?v=1570871051217', CAST(N'2021-04-05T13:54:02.913' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (143, 56, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf3850.jpg?v=1570871052020', CAST(N'2021-04-05T13:54:09.353' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (144, 56, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf3851.jpg?v=1570871052963', CAST(N'2021-04-05T13:54:16.920' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (145, 56, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf3852.jpg?v=1570871053750', CAST(N'2021-04-05T13:54:24.057' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (146, 57, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf3844.jpg?v=1570870921270', CAST(N'2021-04-05T13:54:44.203' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (147, 57, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf3845.jpg?v=1570870922023', CAST(N'2021-04-05T13:54:52.137' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (148, 57, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf3846.jpg?v=1570870924397', CAST(N'2021-04-05T13:54:59.240' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (149, 57, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf3847.jpg?v=1570870925697', CAST(N'2021-04-05T13:55:06.123' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (150, 58, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/113.jpg?v=1547967272397', CAST(N'2021-04-05T13:56:16.917' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (151, 58, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/114.jpg?v=1547967272397', CAST(N'2021-04-05T13:56:23.653' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (152, 58, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/115.jpg?v=1547967272397', CAST(N'2021-04-05T13:56:31.327' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (153, 58, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/116.jpg?v=1547967272397', CAST(N'2021-04-05T13:56:37.410' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (154, 59, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/102.jpg?v=1547967343350', CAST(N'2021-04-05T13:57:17.113' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (155, 59, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/103.jpg?v=1547967343350', CAST(N'2021-04-05T13:57:23.930' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (156, 59, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/104.jpg?v=1547967343350', CAST(N'2021-04-05T13:57:31.587' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (157, 59, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/105.jpg?v=1547967350050', CAST(N'2021-04-05T13:57:40.323' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (158, 62, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/87-f236e24b-fde9-405e-91a3-4dcdb0429374.jpg?v=1547967316320', CAST(N'2021-04-05T13:58:00.217' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (159, 62, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/88-40092ec2-9c6a-4867-8a70-ba056fd33d8f.jpg?v=1547967316993', CAST(N'2021-04-05T13:58:07.387' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (160, 62, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/89-43ebba87-b6d5-4269-8e4e-b09029b4950c.jpg?v=1547967317707', CAST(N'2021-04-05T13:58:17.063' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (161, 62, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/90-2b2dbbba-bcce-48ec-83b1-038112f7fcaa.jpg?v=1547967318317', CAST(N'2021-04-05T13:58:23.810' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (162, 63, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf3650-cfd0ba49-d9cb-4743-a36f-a930cb93c658.jpg?v=1566628717663', CAST(N'2021-04-05T13:58:46.913' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (163, 63, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf3651-79a63de2-99a6-423b-80f2-b8634848f4f2.jpg?v=1566628717663', CAST(N'2021-04-05T13:58:54.087' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (164, 63, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf3652-38613c68-d6e0-4529-a2ae-2711100181ec.jpg?v=1566628717663', CAST(N'2021-04-05T13:58:59.630' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (165, 63, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf3653-ca514aa4-06b2-45fc-a2b9-48740a74e86e.jpg?v=1566628717663', CAST(N'2021-04-05T13:59:06.013' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (166, 64, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6678.jpg?v=1592988531653', CAST(N'2021-04-05T13:59:18.313' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (167, 64, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6679.jpg?v=1592988533490', CAST(N'2021-04-05T13:59:24.683' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (168, 64, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6680.jpg?v=1592988535233', CAST(N'2021-04-05T13:59:31.730' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (169, 64, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6681.jpg?v=1592988537020', CAST(N'2021-04-05T13:59:38.337' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (170, 65, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/46353009-2327007670864064-2118245993139929088-o.jpg?v=1546919915250', CAST(N'2021-04-05T13:59:56.750' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (171, 65, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/46425094-2327006747530823-1323242581668134912-o.jpg?v=1546919915250', CAST(N'2021-04-05T14:00:03.153' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (172, 65, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/46413071-2327006360864195-7519902069260550144-o.jpg?v=1546919915250', CAST(N'2021-04-05T14:00:10.347' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (173, 65, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/46486211-2327007944197370-7576389539267084288-o.jpg?v=1546919915250', CAST(N'2021-04-05T14:00:17.187' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (174, 66, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/46482973-2327008257530672-8317736942217199616-o.jpg?v=1546919915567', CAST(N'2021-04-05T14:00:33.800' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (175, 66, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/46420111-2327007577530740-6101184310222520320-o.jpg?v=1546919915567', CAST(N'2021-04-05T14:00:41.417' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (176, 66, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/46434980-2327007207530777-3781173758677483520-o.jpg?v=1546919915567', CAST(N'2021-04-05T14:00:46.890' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (177, 66, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/46377539-2327008284197336-1270197162877124608-o.jpg?v=1546919915567', CAST(N'2021-04-05T14:00:53.073' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (178, 67, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/16.jpg?v=1546919922130', CAST(N'2021-04-05T14:02:27.623' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (179, 67, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/17.jpg?v=1546919922130', CAST(N'2021-04-05T14:02:34.637' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (180, 67, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/18.jpg?v=1546919922130', CAST(N'2021-04-05T14:02:39.573' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (181, 67, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/19.jpg?v=1546919922130', CAST(N'2021-04-05T14:02:46.020' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (182, 68, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/11.jpg?v=1546919922233', CAST(N'2021-04-05T14:02:57.133' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (183, 68, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/12.jpg?v=1546919922233', CAST(N'2021-04-05T14:03:02.690' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (184, 68, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/13.jpg?v=1546919922233', CAST(N'2021-04-05T14:03:09.280' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (185, 68, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/14.jpg?v=1546919922233', CAST(N'2021-04-05T14:03:16.007' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (186, 69, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/42663248-2297178860513612-7237781713584128000-o-2297178853846946.jpg?v=1546919922333', CAST(N'2021-04-05T14:03:30.980' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (187, 69, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/42669430-2297178037180361-6679355885438894080-o-2297178033847028.jpg?v=1546919922333', CAST(N'2021-04-05T14:03:38.927' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (188, 69, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/42719833-2297178053847026-358641326972993536-o-2297178043847027.jpg?v=1546919922333', CAST(N'2021-04-05T14:03:44.363' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (189, 69, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/42646553-2297179367180228-1783361470310907904-o-2297179360513562.jpg?v=1546919922333', CAST(N'2021-04-05T14:03:50.123' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (190, 72, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9238.jpg?v=1611741894593', CAST(N'2021-04-05T14:04:29.673' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (191, 72, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9239.jpg?v=1611741896127', CAST(N'2021-04-05T14:04:37.153' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (192, 72, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9240.jpg?v=1611741897383', CAST(N'2021-04-05T14:04:45.353' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (193, 72, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9241.jpg?v=1611741898760', CAST(N'2021-04-05T14:04:51.410' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (194, 73, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf4559.jpg?v=1573540616160', CAST(N'2021-04-05T14:05:05.577' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (195, 73, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf4560.jpg?v=1573540617447', CAST(N'2021-04-05T14:05:12.927' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (196, 73, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf4561.jpg?v=1573540618487', CAST(N'2021-04-05T14:05:18.260' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (197, 73, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf4562.jpg?v=1573540619567', CAST(N'2021-04-05T14:05:23.930' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (198, 74, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf4569.jpg?v=1573540759610', CAST(N'2021-04-05T14:05:38.167' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (199, 74, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf4570.jpg?v=1573540760577', CAST(N'2021-04-05T14:05:49.690' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (200, 74, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf4571.jpg?v=1573540761420', CAST(N'2021-04-05T14:05:53.000' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (201, 74, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf4572.jpg?v=1573540762627', CAST(N'2021-04-05T14:06:04.070' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (202, 75, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6560.jpg?v=1592120818850', CAST(N'2021-04-05T14:06:15.770' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (203, 75, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6561.jpg?v=1592120820447', CAST(N'2021-04-05T14:06:23.257' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (204, 75, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6562.jpg?v=1592120821997', CAST(N'2021-04-05T14:06:30.073' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (205, 75, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6563.jpg?v=1592120823547', CAST(N'2021-04-05T14:06:35.860' AS DateTime), NULL)
GO
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (206, 76, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9688.jpg?v=1604392884637', CAST(N'2021-04-05T14:06:59.927' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (207, 76, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9689.jpg?v=1604392887707', CAST(N'2021-04-05T14:07:06.877' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (208, 76, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9690.jpg?v=1604392890763', CAST(N'2021-04-05T14:07:13.013' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (209, 76, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9691.jpg?v=1604392893493', CAST(N'2021-04-05T14:07:18.993' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (210, 77, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9694.jpg?v=1604393030803', CAST(N'2021-04-05T14:07:41.387' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (211, 77, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9695.jpg?v=1604393033690', CAST(N'2021-04-05T14:07:50.690' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (212, 77, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9696.jpg?v=1604393036357', CAST(N'2021-04-05T14:07:58.553' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (213, 77, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9697.jpg?v=1604393039220', CAST(N'2021-04-05T14:08:04.643' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (214, 78, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf4574.jpg?v=1573540720947', CAST(N'2021-04-05T14:08:16.580' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (215, 78, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf4575.jpg?v=1573540721977', CAST(N'2021-04-05T14:08:22.123' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (216, 78, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf4576.jpg?v=1573540723040', CAST(N'2021-04-05T14:08:28.207' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (217, 78, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf4577.jpg?v=1573540724203', CAST(N'2021-04-05T14:08:34.123' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (218, 79, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf4585.jpg?v=1573540651657', CAST(N'2021-04-05T14:08:50.000' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (219, 79, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf4586.jpg?v=1573540652670', CAST(N'2021-04-05T14:10:15.637' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (220, 79, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf4587.jpg?v=1573540653543', CAST(N'2021-04-05T14:10:22.177' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (221, 79, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf4588.jpg?v=1573540654773', CAST(N'2021-04-05T14:10:27.370' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (222, 80, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6655.jpg?v=1599816561337', CAST(N'2021-04-05T14:10:41.307' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (223, 80, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6656.jpg?v=1599816561337', CAST(N'2021-04-05T14:10:48.153' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (224, 80, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6657.jpg?v=1599816561337', CAST(N'2021-04-05T14:10:55.200' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (225, 80, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6658.jpg?v=1599816561337', CAST(N'2021-04-05T14:11:03.090' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (227, 81, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6673.jpg?v=1592988243687', CAST(N'2021-04-05T14:11:57.167' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (228, 81, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6674.jpg?v=1592988245783', CAST(N'2021-04-05T14:12:06.763' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (229, 81, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6675.jpg?v=1592988247387', CAST(N'2021-04-05T14:12:15.300' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (230, 81, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6676.jpg?v=1592988248967', CAST(N'2021-04-05T14:12:22.437' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (231, 82, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6660.jpg?v=1592988268027', CAST(N'2021-04-05T14:12:38.220' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (232, 82, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6661.jpg?v=1592988269917', CAST(N'2021-04-05T14:12:46.353' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (233, 82, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6663.jpg?v=1592988271543', CAST(N'2021-04-05T14:12:54.150' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (235, 82, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6664.jpg?v=1592988273147', CAST(N'2021-04-05T14:13:07.120' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (236, 83, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6668.jpg?v=1592988334557', CAST(N'2021-04-05T14:13:27.047' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (237, 83, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6669.jpg?v=1592988336397', CAST(N'2021-04-05T14:13:32.440' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (238, 83, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6670.jpg?v=1592988338190', CAST(N'2021-04-05T14:13:37.790' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (239, 83, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6671.jpg?v=1592988339977', CAST(N'2021-04-05T14:13:46.353' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (240, 84, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6649.jpg?v=1592988354877', CAST(N'2021-04-05T14:14:05.967' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (241, 84, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6650.jpg?v=1592988356500', CAST(N'2021-04-05T14:14:11.150' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (242, 84, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6651.jpg?v=1592988358067', CAST(N'2021-04-05T14:14:16.617' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (243, 84, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6652.jpg?v=1592988359937', CAST(N'2021-04-05T14:14:24.860' AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[Imagee] OFF
GO
SET IDENTITY_INSERT [dbo].[News] ON 

INSERT [dbo].[News] ([ID], [Title], [Content], [Image], [created_at], [updated_at]) VALUES (1, N'FREE SHIP + SALE', N'<p>SALE + FREE SHIP</p>
<p><span style="color:#ff0000;">- Toàn bộ sản phẩm sale 20% - 30% - 40% - 50%</span></p>
<p><span style="color:#ff0000;">- Free Ship</span></p>
<p><span style="color:#ff0000;">- Giảm 5% cho đôi thứ 2</span></p>
<p><span style="color:#ff0000;">- Tặng hộp nhựa 190k khi mua 3 sản phẩm [Mua hộp nhựa vẫn áp dụng giảm thành viên]</span></p>
<p><span style="color:#000000;">⭐&nbsp;Tất cả sản phẩm đều được kiểm tra trước thanh toán và áp dụng đổi trả trong vòng 7 ngày với giày chưa qua sử dụng&nbsp;không kể lí do [ Đảm bảo 100% sản phẩm gửi đến khách yêu đều là những sản phẩm chất lượng và không lỗi lầm ]</span></p>
<p><span style="color:#000000;">⭐&nbsp;Các bạn có thể đặt nhanh tại website&nbsp;https://tushoes.vn/&nbsp;[ Đặt Hàng Sớm Tránh Tình Trạng Hết Size ]</span></p>
<p><span style="color:#000000;">⭐&nbsp;Đặc Biệt:</span></p>
<p><span style="color:#ff0000;">- Thành Viên&nbsp;: Giảm&nbsp;5%&nbsp;[ Từng mua 1 đôi giày từ Tu Shoes ]</span></p>
<p><span style="color:#ff0000;">- Thành Viên Bạc: Giảm&nbsp;10%&nbsp;[ Từng mua 3 đôi giày từ Tu Shoes ]</span></p>
<p><span style="color:#ff0000;">- Thành Viên Vàng: Giảm&nbsp;15%&nbsp;[ Từng mua 7 đôi giày từ Tu Shoes ]</span></p>
<p><span style="color:#ff0000;">- Thành Viên Kim Cương: Giảm&nbsp;20%&nbsp;[ Từng mua 15 đôi giày từ Tu Shoes ]</span></p>
<p><span style="color:#000000;">Chi tiết chương trình cộng dồn sale cho&nbsp;thành viên:&nbsp;https://tushoes.vn/the-thanh-vien-tu-shoes</span></p>
<p><span style="color:#000000;">❌ Lưu ý:</span></p>
<p><span style="color:#000000;">- Chương trình áp dụng cộng dồn thành viên [ những sp sale 50% không cộn dồn ]</span></p>
<p><span style="color:#000000;">- Không hỗ trợ giữ size hay giữ mẫu.</span></p>
<p>&nbsp;</p>
<p><span style="color:#000000;">#TuShoes</span></p>', N'https://bizweb.dktcdn.net/100/336/177/articles/154030916-619301002229112-3523518870344352676-n.jpg?v=1614327956173', CAST(N'2021-04-02T08:39:35.333' AS DateTime), NULL)
INSERT [dbo].[News] ([ID], [Title], [Content], [Image], [created_at], [updated_at]) VALUES (2, NULL, N'<h1 class="article-title"><a href="/giay-rep-1-1-chuan-gia-re-nhat-tu-shoes" itemprop="name">Giày Rep 1:1 Chuẩn Giá Rẻ Nhất - Tu Shoes</a></h1>
						<div class="post-time">
							Viết bởi <span>Hồ Sỹ Tú</span>, Ngày 07/12/2020
						</div>
						<div class="article-details">
							<div class="article-image">
								<a href="/giay-rep-1-1-chuan-gia-re-nhat-tu-shoes">
									
									<img itemprop="image" class="img-fluid" src="//bizweb.dktcdn.net/100/336/177/articles/6759752f7e408f1ed651.jpg?v=1607336167230" alt="Giày Rep 1:1 Chuẩn Giá Rẻ Nhất - Tu Shoes">
									

								</a>
							</div>							
							<div class="article-content" itemprop="description">
								<div class="rte">
									<p><span style="font-size:18px;"><u><strong><span style="color:#000000;">Vì Sao Giày Chuẩn Rep 1:1 Tu Shoes Lại Rẻ Nhất ?</span></strong></u></span></p>
<p><span style="color:#000000;"><strong>1. Nhập hàng trực tiếp từ nhà máy.</strong></span></p>
<p><span style="color:#000000;">Toàn bộ sản phẩm Tu Shoes đều được nhập trực tiếp từ nhà máy. Không thông qua trung gian cũng như là kiểm tra về chất lượng sản phẩm nghiêm ngặt qua nhiều bước nên Tu Shoes luôn cam kết mang đến chất lượng tốt nhất với giá tốt nhất.</span></p>
<p><span style="color:#000000;"><img data-thumb="original" original-height="1440" original-width="1080" src="//bizweb.dktcdn.net/100/336/177/files/1233211.jpg?v=1607334652468" /><img data-thumb="original" original-height="858" original-width="858" src="//bizweb.dktcdn.net/100/336/177/files/12-6ccd18b5-c060-450c-a1e1-a354ac3c35f8.jpg?v=1607334923653" /><img data-thumb="original" original-height="520" original-width="780" src="//bizweb.dktcdn.net/100/336/177/files/123.jpg?v=1607334932830" /><img data-thumb="original" original-height="1440" original-width="1080" src="//bizweb.dktcdn.net/100/336/177/files/1234.jpg?v=1607334938865" /></span></p>
<p><span style="color:#000000;"><strong>2. Đúng Giá Đúng Chất Lượng [ Nói Không Với Lừa Đảo, Bán Hàng Rác , Trộn Hàng Hoặc Đôn Hàng ]</strong></span></p>
<p><span style="color:#000000;">Mọi người hay so sánh đặt câu hỏi là vì sao cửa hàng này đắt hơn cửa hàng khác 200k, 300k hay thậm chí là 500k ? Đơn giản vì&nbsp;<strong>chất lượng hoàn toàn khác nhau</strong>&nbsp;. Tu Shoes đã làm việc trong ngành giày hơn 3 năm và hiểu rõ điều này. Nếu cửa hàng khác bán chất lượng tương tự mà giá rẻ hơn rất nhiều vậy tại sao Tu Shoes không sang bên cửa hàng đó nhập hết hàng bên đó về và bán giá cao để kiếm lời ? Chỉ đơn giản là&nbsp;<strong>chất lượng hoàn toàn khác nhau cho nên giá tiền cũng hoàn toàn khác nhau&nbsp;</strong>và những cửa hàng bán hàng rẻ tiền quảng cáo chất lượng Rep 1:1 chuẩn là đang&nbsp;<strong>lừa đảo khách hàng.</strong></span></p>
<p>&nbsp;</p>
<p><span style="color:#000000;"><strong>Ví Dụ:&nbsp;</strong>Khách hàng đặt 1 sản phẩm A của Tu Shoes thì shop sẽ mất thời gian khoảng 7-10 ngày để sản phẩm đó có thể vận chuyển từ <b>Nhà Máy&nbsp;</b>về đến shop để giao hàng hóa hay sản phẩm chất lượng cho bạn khách hàng.</span></p>
<p><span style="color:#000000;">Nếu những cửa hàng khác&nbsp;cũng bán hàng rep 1:1 chất lượng tốt mà giá rẻ tại sao bên shop không qua đó lấy hàng chất lượng tốt giá rẻ như quảng cáo để trả hàng cho khách, không phải bắt khách đợi 7-10 ngày nữa&nbsp;?&nbsp;<strong>Chỉ đơn giản là những cửa hàng đó đang lừa đảo khách hàng và hàng hóa đó không đủ tiêu chuẩn Rep 1:1 như đã quảng cáo để mang đến cho khách hàng những chất lượng tốt nhất.</strong></span></p>
<p><span style="color:#000000;"><strong><u>Vì thế đúng giá rẻ nhất và đúng chất lượng tốt nhất chỉ có tại Tu Shoes</u></strong></span></p>
<p>&nbsp;</p>
<p><span style="color:#000000;"><strong>3. Cửa hàng Uy Tín.</strong></span></p>
<p><span style="color:#000000;">&nbsp;Với 1 cửa hàng uy tín thì thực hiện đầy đủ cam kết với khách hàng từ bảo hành hàng hóa, đổi trả hàng hóa cũng như việc nâng cao chất lượng phục vụ là điều quan trọng nhất.</span></p>
<p><span style="color:#000000;">Với TuShoes bạn có thể:</span></p>
<p><span style="color:#000000;">&nbsp;- <b>Đổi Hoặc Trả</b>&nbsp;sản phẩm trong 7 ngày [ Với giày chưa sử dụng không kể lí do&nbsp;]</span></p>
<p><span style="color:#000000;">Bạn sẽ có 7 ngày để kiểm tra chất lượng sản phẩm, bạn có thể mang giày đi mọi cửa hàng khác để có thể so sánh về chất lượng, so sánh về giá và nếu bạn thấy bên cửa hàng Tu Shoes sai về cam kết chất lượng tốt nhất giá tốt nhất thì bạn cứ mang trả lại cho Tu Shoes.</span></p>
<p><span style="color:#000000;">-&nbsp;<strong>Bảo hành 1 đổi 1 t</strong>rong 3 tháng với lỗi từ nhà sản xuất</span></p>
<p><span style="color:#000000;">Với bất kì lỗi nào từ nhà máy như chất lượng không đảm bảo dẫn đến việc bung keo thì bên mình hỗ trợ nhận lại giày và đổi cho khách hàng sản phẩm mới.</span></p>
<p><span style="color:#000000;">-&nbsp;<strong>Hỗ trợ bảo hành và sửa chữa miễn phí</strong></span></p>
<p><span style="color:#000000;">Với những sản phẩm sau 3 tháng mà gặp lỗi từ giày thì bên shop vẫn sẽ nhận về và hỗ trợ sửa lại giày miễn phí cho khách hàng.</span></p>
<p><span style="color:#000000;"><strong>4. Kênh Thông Tin:</strong></span></p>
<p><span style="color:#000000;">TuShoes có kênh youtube giúp mọi người có thể có cái nhìn tổng quát về chất lượng sản phẩm cũng như việc so sánh các chất lượng khác nhau và kiểm tra chất lượng giày mà mọi người đang sử dụng: </span><u><strong><a href="https://www.youtube.com/channel/UCz-dKtYr02q7FFC3NF5-AQw?view_as=subscriber"><span style="color:#ff0000;">youtube Tu Shoes</span></a></strong></u></p>
<p>&nbsp;</p>
<p><span style="color:#000000;"><strong><u>Bên trên là những kinh nghiệm của Tu Shoes và cam kết của Tu Shoes cũng như hướng dẫn mọi người kinh nghiệm mua sắm và đảm bảo mọi người sẽ mua được sản phẩm đúng với số tiền mà mọi người bỏ ra. Cảm ơn bạn đã xem hết bài viết.</u></strong></span></p>
								</div>
							</div>
						</div>
	<h1 class="article-title"><a href="/giay-rep-1-1-chuan-gia-re-nhat-tu-shoes" itemprop="name">Giày Rep 1:1 Chuẩn Giá Rẻ Nhất - Tu Shoes</a></h1>
						<div class="post-time">
							Viết bởi <span>Hồ Sỹ Tú</span>, Ngày 07/12/2020
						</div>
						<div class="article-details">
							<div class="article-image">
								<a href="/giay-rep-1-1-chuan-gia-re-nhat-tu-shoes">
									
									<img itemprop="image" class="img-fluid" src="//bizweb.dktcdn.net/100/336/177/articles/6759752f7e408f1ed651.jpg?v=1607336167230" alt="Giày Rep 1:1 Chuẩn Giá Rẻ Nhất - Tu Shoes">
									

								</a>
							</div>							
							<div class="article-content" itemprop="description">
								<div class="rte">
									<p><span style="font-size:18px;"><u><strong><span style="color:#000000;">Vì Sao Giày Chuẩn Rep 1:1 Tu Shoes Lại Rẻ Nhất ?</span></strong></u></span></p>
<p><span style="color:#000000;"><strong>1. Nhập hàng trực tiếp từ nhà máy.</strong></span></p>
<p><span style="color:#000000;">Toàn bộ sản phẩm Tu Shoes đều được nhập trực tiếp từ nhà máy. Không thông qua trung gian cũng như là kiểm tra về chất lượng sản phẩm nghiêm ngặt qua nhiều bước nên Tu Shoes luôn cam kết mang đến chất lượng tốt nhất với giá tốt nhất.</span></p>
<p><span style="color:#000000;"><img data-thumb="original" original-height="1440" original-width="1080" src="//bizweb.dktcdn.net/100/336/177/files/1233211.jpg?v=1607334652468" /><img data-thumb="original" original-height="858" original-width="858" src="//bizweb.dktcdn.net/100/336/177/files/12-6ccd18b5-c060-450c-a1e1-a354ac3c35f8.jpg?v=1607334923653" /><img data-thumb="original" original-height="520" original-width="780" src="//bizweb.dktcdn.net/100/336/177/files/123.jpg?v=1607334932830" /><img data-thumb="original" original-height="1440" original-width="1080" src="//bizweb.dktcdn.net/100/336/177/files/1234.jpg?v=1607334938865" /></span></p>
<p><span style="color:#000000;"><strong>2. Đúng Giá Đúng Chất Lượng [ Nói Không Với Lừa Đảo, Bán Hàng Rác , Trộn Hàng Hoặc Đôn Hàng ]</strong></span></p>
<p><span style="color:#000000;">Mọi người hay so sánh đặt câu hỏi là vì sao cửa hàng này đắt hơn cửa hàng khác 200k, 300k hay thậm chí là 500k ? Đơn giản vì&nbsp;<strong>chất lượng hoàn toàn khác nhau</strong>&nbsp;. Tu Shoes đã làm việc trong ngành giày hơn 3 năm và hiểu rõ điều này. Nếu cửa hàng khác bán chất lượng tương tự mà giá rẻ hơn rất nhiều vậy tại sao Tu Shoes không sang bên cửa hàng đó nhập hết hàng bên đó về và bán giá cao để kiếm lời ? Chỉ đơn giản là&nbsp;<strong>chất lượng hoàn toàn khác nhau cho nên giá tiền cũng hoàn toàn khác nhau&nbsp;</strong>và những cửa hàng bán hàng rẻ tiền quảng cáo chất lượng Rep 1:1 chuẩn là đang&nbsp;<strong>lừa đảo khách hàng.</strong></span></p>
<p>&nbsp;</p>
<p><span style="color:#000000;"><strong>Ví Dụ:&nbsp;</strong>Khách hàng đặt 1 sản phẩm A của Tu Shoes thì shop sẽ mất thời gian khoảng 7-10 ngày để sản phẩm đó có thể vận chuyển từ <b>Nhà Máy&nbsp;</b>về đến shop để giao hàng hóa hay sản phẩm chất lượng cho bạn khách hàng.</span></p>
<p><span style="color:#000000;">Nếu những cửa hàng khác&nbsp;cũng bán hàng rep 1:1 chất lượng tốt mà giá rẻ tại sao bên shop không qua đó lấy hàng chất lượng tốt giá rẻ như quảng cáo để trả hàng cho khách, không phải bắt khách đợi 7-10 ngày nữa&nbsp;?&nbsp;<strong>Chỉ đơn giản là những cửa hàng đó đang lừa đảo khách hàng và hàng hóa đó không đủ tiêu chuẩn Rep 1:1 như đã quảng cáo để mang đến cho khách hàng những chất lượng tốt nhất.</strong></span></p>
<p><span style="color:#000000;"><strong><u>Vì thế đúng giá rẻ nhất và đúng chất lượng tốt nhất chỉ có tại Tu Shoes</u></strong></span></p>
<p>&nbsp;</p>
<p><span style="color:#000000;"><strong>3. Cửa hàng Uy Tín.</strong></span></p>
<p><span style="color:#000000;">&nbsp;Với 1 cửa hàng uy tín thì thực hiện đầy đủ cam kết với khách hàng từ bảo hành hàng hóa, đổi trả hàng hóa cũng như việc nâng cao chất lượng phục vụ là điều quan trọng nhất.</span></p>
<p><span style="color:#000000;">Với TuShoes bạn có thể:</span></p>
<p><span style="color:#000000;">&nbsp;- <b>Đổi Hoặc Trả</b>&nbsp;sản phẩm trong 7 ngày [ Với giày chưa sử dụng không kể lí do&nbsp;]</span></p>
<p><span style="color:#000000;">Bạn sẽ có 7 ngày để kiểm tra chất lượng sản phẩm, bạn có thể mang giày đi mọi cửa hàng khác để có thể so sánh về chất lượng, so sánh về giá và nếu bạn thấy bên cửa hàng Tu Shoes sai về cam kết chất lượng tốt nhất giá tốt nhất thì bạn cứ mang trả lại cho Tu Shoes.</span></p>
<p><span style="color:#000000;">-&nbsp;<strong>Bảo hành 1 đổi 1 t</strong>rong 3 tháng với lỗi từ nhà sản xuất</span></p>
<p><span style="color:#000000;">Với bất kì lỗi nào từ nhà máy như chất lượng không đảm bảo dẫn đến việc bung keo thì bên mình hỗ trợ nhận lại giày và đổi cho khách hàng sản phẩm mới.</span></p>
<p><span style="color:#000000;">-&nbsp;<st</', NULL, NULL, NULL)
INSERT [dbo].[News] ([ID], [Title], [Content], [Image], [created_at], [updated_at]) VALUES (3, N' Shoes', N'<h1 class="article-title"><a href="/giay-rep-1-1-chuan-gia-re-nhat-tu-shoes" itemprop="name">Giày Rep 1:1 Chuẩn Giá Rẻ Nhất - Tu Shoes</a></h1>
						<div class="post-time">
							Viết bởi <span>Hồ Sỹ Tú</span>, Ngày 07/12/2020
						</div>
						<div class="article-details">
							<div class="article-image">
								<a href="/giay-rep-1-1-chuan-gia-re-nhat-tu-shoes">
									
									<img itemprop="image" class="img-fluid" src="//bizweb.dktcdn.net/100/336/177/articles/6759752f7e408f1ed651.jpg?v=1607336167230" alt="Giày Rep 1:1 Chuẩn Giá Rẻ Nhất - Tu Shoes">
									

								</a>
							</div>							
							<div class="article-content" itemprop="description">
								<div class="rte">
									<p><span style="font-size:18px;"><u><strong><span style="color:#000000;">Vì Sao Giày Chuẩn Rep 1:1 Tu Shoes Lại Rẻ Nhất ?</span></strong></u></span></p>
<p><span style="color:#000000;"><strong>1. Nhập hàng trực tiếp từ nhà máy.</strong></span></p>
<p><span style="color:#000000;">Toàn bộ sản phẩm Tu Shoes đều được nhập trực tiếp từ nhà máy. Không thông qua trung gian cũng như là kiểm tra về chất lượng sản phẩm nghiêm ngặt qua nhiều bước nên Tu Shoes luôn cam kết mang đến chất lượng tốt nhất với giá tốt nhất.</span></p>
<p><span style="color:#000000;"><img data-thumb="original" original-height="1440" original-width="1080" src="//bizweb.dktcdn.net/100/336/177/files/1233211.jpg?v=1607334652468" /><img data-thumb="original" original-height="858" original-width="858" src="//bizweb.dktcdn.net/100/336/177/files/12-6ccd18b5-c060-450c-a1e1-a354ac3c35f8.jpg?v=1607334923653" /><img data-thumb="original" original-height="520" original-width="780" src="//bizweb.dktcdn.net/100/336/177/files/123.jpg?v=1607334932830" /><img data-thumb="original" original-height="1440" original-width="1080" src="//bizweb.dktcdn.net/100/336/177/files/1234.jpg?v=1607334938865" /></span></p>
<p><span style="color:#000000;"><strong>2. Đúng Giá Đúng Chất Lượng [ Nói Không Với Lừa Đảo, Bán Hàng Rác , Trộn Hàng Hoặc Đôn Hàng ]</strong></span></p>
<p><span style="color:#000000;">Mọi người hay so sánh đặt câu hỏi là vì sao cửa hàng này đắt hơn cửa hàng khác 200k, 300k hay thậm chí là 500k ? Đơn giản vì&nbsp;<strong>chất lượng hoàn toàn khác nhau</strong>&nbsp;. Tu Shoes đã làm việc trong ngành giày hơn 3 năm và hiểu rõ điều này. Nếu cửa hàng khác bán chất lượng tương tự mà giá rẻ hơn rất nhiều vậy tại sao Tu Shoes không sang bên cửa hàng đó nhập hết hàng bên đó về và bán giá cao để kiếm lời ? Chỉ đơn giản là&nbsp;<strong>chất lượng hoàn toàn khác nhau cho nên giá tiền cũng hoàn toàn khác nhau&nbsp;</strong>và những cửa hàng bán hàng rẻ tiền quảng cáo chất lượng Rep 1:1 chuẩn là đang&nbsp;<strong>lừa đảo khách hàng.</strong></span></p>
<p>&nbsp;</p>
<p><span style="color:#000000;"><strong>Ví Dụ:&nbsp;</strong>Khách hàng đặt 1 sản phẩm A của Tu Shoes thì shop sẽ mất thời gian khoảng 7-10 ngày để sản phẩm đó có thể vận chuyển từ <b>Nhà Máy&nbsp;</b>về đến shop để giao hàng hóa hay sản phẩm chất lượng cho bạn khách hàng.</span></p>
<p><span style="color:#000000;">Nếu những cửa hàng khác&nbsp;cũng bán hàng rep 1:1 chất lượng tốt mà giá rẻ tại sao bên shop không qua đó lấy hàng chất lượng tốt giá rẻ như quảng cáo để trả hàng cho khách, không phải bắt khách đợi 7-10 ngày nữa&nbsp;?&nbsp;<strong>Chỉ đơn giản là những cửa hàng đó đang lừa đảo khách hàng và hàng hóa đó không đủ tiêu chuẩn Rep 1:1 như đã quảng cáo để mang đến cho khách hàng những chất lượng tốt nhất.</strong></span></p>
<p><span style="color:#000000;"><strong><u>Vì thế đúng giá rẻ nhất và đúng chất lượng tốt nhất chỉ có tại Tu Shoes</u></strong></span></p>
<p>&nbsp;</p>
<p><span style="color:#000000;"><strong>3. Cửa hàng Uy Tín.</strong></span></p>
<p><span style="color:#000000;">&nbsp;Với 1 cửa hàng uy tín thì thực hiện đầy đủ cam kết với khách hàng từ bảo hành hàng hóa, đổi trả hàng hóa cũng như việc nâng cao chất lượng phục vụ là điều quan trọng nhất.</span></p>
<p><span style="color:#000000;">Với TuShoes bạn có thể:</span></p>
<p><span style="color:#000000;">&nbsp;- <b>Đổi Hoặc Trả</b>&nbsp;sản phẩm trong 7 ngày [ Với giày chưa sử dụng không kể lí do&nbsp;]</span></p>
<p><span style="color:#000000;">Bạn sẽ có 7 ngày để kiểm tra chất lượng sản phẩm, bạn có thể mang giày đi mọi cửa hàng khác để có thể so sánh về chất lượng, so sánh về giá và nếu bạn thấy bên cửa hàng Tu Shoes sai về cam kết chất lượng tốt nhất giá tốt nhất thì bạn cứ mang trả lại cho Tu Shoes.</span></p>
<p><span style="color:#000000;">-&nbsp;<strong>Bảo hành 1 đổi 1 t</strong>rong 3 tháng với lỗi từ nhà sản xuất</span></p>
<p><span style="color:#000000;">Với bất kì lỗi nào từ nhà máy như chất lượng không đảm bảo dẫn đến việc bung keo thì bên mình hỗ trợ nhận lại giày và đổi cho khách hàng sản phẩm mới.</span></p>
<p><span style="color:#000000;">-&nbsp;<strong>Hỗ trợ bảo hành và sửa chữa miễn phí</strong></span></p>
<p><span style="color:#000000;">Với những sản phẩm sau 3 tháng mà gặp lỗi từ giày thì bên shop vẫn sẽ nhận về và hỗ trợ sửa lại giày miễn phí cho khách hàng.</span></p>
<p><span style="color:#000000;"><strong>4. Kênh Thông Tin:</strong></span></p>
<p><span style="color:#000000;">TuShoes có kênh youtube giúp mọi người có thể có cái nhìn tổng quát về chất lượng sản phẩm cũng như việc so sánh các chất lượng khác nhau và kiểm tra chất lượng giày mà mọi người đang sử dụng: </span><u><strong><a href="https://www.youtube.com/channel/UCz-dKtYr02q7FFC3NF5-AQw?view_as=subscriber"><span style="color:#ff0000;">youtube Tu Shoes</span></a></strong></u></p>
<p>&nbsp;</p>
<p><span style="color:#000000;"><strong><u>Bên trên là những kinh nghiệm của Tu Shoes và cam kết của Tu Shoes cũng như hướng dẫn mọi người kinh nghiệm mua sắm và đảm bảo mọi người sẽ mua được sản phẩm đúng với số tiền mà mọi người bỏ ra. Cảm ơn bạn đã xem hết bài viết.</u></strong></span></p>
								</div>
							</div>
						</div>
					</div>', N'https://bizweb.dktcdn.net/100/336/177/articles/6759752f7e408f1ed651.jpg?v=1607336167230', NULL, NULL)
INSERT [dbo].[News] ([ID], [Title], [Content], [Image], [created_at], [updated_at]) VALUES (4, N' Shoes', N'<h1 class="article-title"><a href="/giay-rep-1-1-chuan-gia-re-nhat-tu-shoes" itemprop="name">Giày Rep 1:1 Chuẩn Giá Rẻ Nhất - Tu Shoes</a></h1>
						<div class="post-time">
							Viết bởi <span>Hồ Sỹ Tú</span>, Ngày 07/12/2020
						</div>
						<div class="article-details">
							<div class="article-image">
								<a href="/giay-rep-1-1-chuan-gia-re-nhat-tu-shoes">
									
									<img itemprop="image" class="img-fluid" src="//bizweb.dktcdn.net/100/336/177/articles/6759752f7e408f1ed651.jpg?v=1607336167230" alt="Giày Rep 1:1 Chuẩn Giá Rẻ Nhất - Tu Shoes">
									

								</a>
							</div>							
							<div class="article-content" itemprop="description">
								<div class="rte">
									<p><span style="font-size:18px;"><u><strong><span style="color:#000000;">Vì Sao Giày Chuẩn Rep 1:1 Tu Shoes Lại Rẻ Nhất ?</span></strong></u></span></p>
<p><span style="color:#000000;"><strong>1. Nhập hàng trực tiếp từ nhà máy.</strong></span></p>
<p><span style="color:#000000;">Toàn bộ sản phẩm Tu Shoes đều được nhập trực tiếp từ nhà máy. Không thông qua trung gian cũng như là kiểm tra về chất lượng sản phẩm nghiêm ngặt qua nhiều bước nên Tu Shoes luôn cam kết mang đến chất lượng tốt nhất với giá tốt nhất.</span></p>
<p><span style="color:#000000;"><img data-thumb="original" original-height="1440" original-width="1080" src="//bizweb.dktcdn.net/100/336/177/files/1233211.jpg?v=1607334652468" /><img data-thumb="original" original-height="858" original-width="858" src="//bizweb.dktcdn.net/100/336/177/files/12-6ccd18b5-c060-450c-a1e1-a354ac3c35f8.jpg?v=1607334923653" /><img data-thumb="original" original-height="520" original-width="780" src="//bizweb.dktcdn.net/100/336/177/files/123.jpg?v=1607334932830" /><img data-thumb="original" original-height="1440" original-width="1080" src="//bizweb.dktcdn.net/100/336/177/files/1234.jpg?v=1607334938865" /></span></p>
<p><span style="color:#000000;"><strong>2. Đúng Giá Đúng Chất Lượng [ Nói Không Với Lừa Đảo, Bán Hàng Rác , Trộn Hàng Hoặc Đôn Hàng ]</strong></span></p>
<p><span style="color:#000000;">Mọi người hay so sánh đặt câu hỏi là vì sao cửa hàng này đắt hơn cửa hàng khác 200k, 300k hay thậm chí là 500k ? Đơn giản vì&nbsp;<strong>chất lượng hoàn toàn khác nhau</strong>&nbsp;. Tu Shoes đã làm việc trong ngành giày hơn 3 năm và hiểu rõ điều này. Nếu cửa hàng khác bán chất lượng tương tự mà giá rẻ hơn rất nhiều vậy tại sao Tu Shoes không sang bên cửa hàng đó nhập hết hàng bên đó về và bán giá cao để kiếm lời ? Chỉ đơn giản là&nbsp;<strong>chất lượng hoàn toàn khác nhau cho nên giá tiền cũng hoàn toàn khác nhau&nbsp;</strong>và những cửa hàng bán hàng rẻ tiền quảng cáo chất lượng Rep 1:1 chuẩn là đang&nbsp;<strong>lừa đảo khách hàng.</strong></span></p>
<p>&nbsp;</p>
<p><span style="color:#000000;"><strong>Ví Dụ:&nbsp;</strong>Khách hàng đặt 1 sản phẩm A của Tu Shoes thì shop sẽ mất thời gian khoảng 7-10 ngày để sản phẩm đó có thể vận chuyển từ <b>Nhà Máy&nbsp;</b>về đến shop để giao hàng hóa hay sản phẩm chất lượng cho bạn khách hàng.</span></p>
<p><span style="color:#000000;">Nếu những cửa hàng khác&nbsp;cũng bán hàng rep 1:1 chất lượng tốt mà giá rẻ tại sao bên shop không qua đó lấy hàng chất lượng tốt giá rẻ như quảng cáo để trả hàng cho khách, không phải bắt khách đợi 7-10 ngày nữa&nbsp;?&nbsp;<strong>Chỉ đơn giản là những cửa hàng đó đang lừa đảo khách hàng và hàng hóa đó không đủ tiêu chuẩn Rep 1:1 như đã quảng cáo để mang đến cho khách hàng những chất lượng tốt nhất.</strong></span></p>
<p><span style="color:#000000;"><strong><u>Vì thế đúng giá rẻ nhất và đúng chất lượng tốt nhất chỉ có tại Tu Shoes</u></strong></span></p>
<p>&nbsp;</p>
<p><span style="color:#000000;"><strong>3. Cửa hàng Uy Tín.</strong></span></p>
<p><span style="color:#000000;">&nbsp;Với 1 cửa hàng uy tín thì thực hiện đầy đủ cam kết với khách hàng từ bảo hành hàng hóa, đổi trả hàng hóa cũng như việc nâng cao chất lượng phục vụ là điều quan trọng nhất.</span></p>
<p><span style="color:#000000;">Với TuShoes bạn có thể:</span></p>
<p><span style="color:#000000;">&nbsp;- <b>Đổi Hoặc Trả</b>&nbsp;sản phẩm trong 7 ngày [ Với giày chưa sử dụng không kể lí do&nbsp;]</span></p>
<p><span style="color:#000000;">Bạn sẽ có 7 ngày để kiểm tra chất lượng sản phẩm, bạn có thể mang giày đi mọi cửa hàng khác để có thể so sánh về chất lượng, so sánh về giá và nếu bạn thấy bên cửa hàng Tu Shoes sai về cam kết chất lượng tốt nhất giá tốt nhất thì bạn cứ mang trả lại cho Tu Shoes.</span></p>
<p><span style="color:#000000;">-&nbsp;<strong>Bảo hành 1 đổi 1 t</strong>rong 3 tháng với lỗi từ nhà sản xuất</span></p>
<p><span style="color:#000000;">Với bất kì lỗi nào từ nhà máy như chất lượng không đảm bảo dẫn đến việc bung keo thì bên mình hỗ trợ nhận lại giày và đổi cho khách hàng sản phẩm mới.</span></p>
<p><span style="color:#000000;">-&nbsp;<strong>Hỗ trợ bảo hành và sửa chữa miễn phí</strong></span></p>
<p><span style="color:#000000;">Với những sản phẩm sau 3 tháng mà gặp lỗi từ giày thì bên shop vẫn sẽ nhận về và hỗ trợ sửa lại giày miễn phí cho khách hàng.</span></p>
<p><span style="color:#000000;"><strong>4. Kênh Thông Tin:</strong></span></p>
<p><span style="color:#000000;">TuShoes có kênh youtube giúp mọi người có thể có cái nhìn tổng quát về chất lượng sản phẩm cũng như việc so sánh các chất lượng khác nhau và kiểm tra chất lượng giày mà mọi người đang sử dụng: </span><u><strong><a href="https://www.youtube.com/channel/UCz-dKtYr02q7FFC3NF5-AQw?view_as=subscriber"><span style="color:#ff0000;">youtube Tu Shoes</span></a></strong></u></p>
<p>&nbsp;</p>
<p><span style="color:#000000;"><strong><u>Bên trên là những kinh nghiệm của Tu Shoes và cam kết của Tu Shoes cũng như hướng dẫn mọi người kinh nghiệm mua sắm và đảm bảo mọi người sẽ mua được sản phẩm đúng với số tiền mà mọi người bỏ ra. Cảm ơn bạn đã xem hết bài viết.</u></strong></span></p>
								</div>
							</div>
						</div>
					</div>', N'https://bizweb.dktcdn.net/100/336/177/articles/6759752f7e408f1ed651.jpg?v=1607336167230', NULL, NULL)
INSERT [dbo].[News] ([ID], [Title], [Content], [Image], [created_at], [updated_at]) VALUES (5, N' Shoes', N'<h1 class="article-title"><a href="/giay-rep-1-1-chuan-gia-re-nhat-tu-shoes" itemprop="name">Giày Rep 1:1 Chuẩn Giá Rẻ Nhất - Tu Shoes</a></h1>
						<div class="post-time">
							Viết bởi <span>Hồ Sỹ Tú</span>, Ngày 07/12/2020
						</div>
						<div class="article-details">
							<div class="article-image">
								<a href="/giay-rep-1-1-chuan-gia-re-nhat-tu-shoes">
									
									<img itemprop="image" class="img-fluid" src="//bizweb.dktcdn.net/100/336/177/articles/6759752f7e408f1ed651.jpg?v=1607336167230" alt="Giày Rep 1:1 Chuẩn Giá Rẻ Nhất - Tu Shoes">
									

								</a>
							</div>							
							<div class="article-content" itemprop="description">
								<div class="rte">
									<p><span style="font-size:18px;"><u><strong><span style="color:#000000;">Vì Sao Giày Chuẩn Rep 1:1 Tu Shoes Lại Rẻ Nhất ?</span></strong></u></span></p>
<p><span style="color:#000000;"><strong>1. Nhập hàng trực tiếp từ nhà máy.</strong></span></p>
<p><span style="color:#000000;">Toàn bộ sản phẩm Tu Shoes đều được nhập trực tiếp từ nhà máy. Không thông qua trung gian cũng như là kiểm tra về chất lượng sản phẩm nghiêm ngặt qua nhiều bước nên Tu Shoes luôn cam kết mang đến chất lượng tốt nhất với giá tốt nhất.</span></p>
<p><span style="color:#000000;"><img data-thumb="original" original-height="1440" original-width="1080" src="//bizweb.dktcdn.net/100/336/177/files/1233211.jpg?v=1607334652468" /><img data-thumb="original" original-height="858" original-width="858" src="//bizweb.dktcdn.net/100/336/177/files/12-6ccd18b5-c060-450c-a1e1-a354ac3c35f8.jpg?v=1607334923653" /><img data-thumb="original" original-height="520" original-width="780" src="//bizweb.dktcdn.net/100/336/177/files/123.jpg?v=1607334932830" /><img data-thumb="original" original-height="1440" original-width="1080" src="//bizweb.dktcdn.net/100/336/177/files/1234.jpg?v=1607334938865" /></span></p>
<p><span style="color:#000000;"><strong>2. Đúng Giá Đúng Chất Lượng [ Nói Không Với Lừa Đảo, Bán Hàng Rác , Trộn Hàng Hoặc Đôn Hàng ]</strong></span></p>
<p><span style="color:#000000;">Mọi người hay so sánh đặt câu hỏi là vì sao cửa hàng này đắt hơn cửa hàng khác 200k, 300k hay thậm chí là 500k ? Đơn giản vì&nbsp;<strong>chất lượng hoàn toàn khác nhau</strong>&nbsp;. Tu Shoes đã làm việc trong ngành giày hơn 3 năm và hiểu rõ điều này. Nếu cửa hàng khác bán chất lượng tương tự mà giá rẻ hơn rất nhiều vậy tại sao Tu Shoes không sang bên cửa hàng đó nhập hết hàng bên đó về và bán giá cao để kiếm lời ? Chỉ đơn giản là&nbsp;<strong>chất lượng hoàn toàn khác nhau cho nên giá tiền cũng hoàn toàn khác nhau&nbsp;</strong>và những cửa hàng bán hàng rẻ tiền quảng cáo chất lượng Rep 1:1 chuẩn là đang&nbsp;<strong>lừa đảo khách hàng.</strong></span></p>
<p>&nbsp;</p>
<p><span style="color:#000000;"><strong>Ví Dụ:&nbsp;</strong>Khách hàng đặt 1 sản phẩm A của Tu Shoes thì shop sẽ mất thời gian khoảng 7-10 ngày để sản phẩm đó có thể vận chuyển từ <b>Nhà Máy&nbsp;</b>về đến shop để giao hàng hóa hay sản phẩm chất lượng cho bạn khách hàng.</span></p>
<p><span style="color:#000000;">Nếu những cửa hàng khác&nbsp;cũng bán hàng rep 1:1 chất lượng tốt mà giá rẻ tại sao bên shop không qua đó lấy hàng chất lượng tốt giá rẻ như quảng cáo để trả hàng cho khách, không phải bắt khách đợi 7-10 ngày nữa&nbsp;?&nbsp;<strong>Chỉ đơn giản là những cửa hàng đó đang lừa đảo khách hàng và hàng hóa đó không đủ tiêu chuẩn Rep 1:1 như đã quảng cáo để mang đến cho khách hàng những chất lượng tốt nhất.</strong></span></p>
<p><span style="color:#000000;"><strong><u>Vì thế đúng giá rẻ nhất và đúng chất lượng tốt nhất chỉ có tại Tu Shoes</u></strong></span></p>
<p>&nbsp;</p>
<p><span style="color:#000000;"><strong>3. Cửa hàng Uy Tín.</strong></span></p>
<p><span style="color:#000000;">&nbsp;Với 1 cửa hàng uy tín thì thực hiện đầy đủ cam kết với khách hàng từ bảo hành hàng hóa, đổi trả hàng hóa cũng như việc nâng cao chất lượng phục vụ là điều quan trọng nhất.</span></p>
<p><span style="color:#000000;">Với TuShoes bạn có thể:</span></p>
<p><span style="color:#000000;">&nbsp;- <b>Đổi Hoặc Trả</b>&nbsp;sản phẩm trong 7 ngày [ Với giày chưa sử dụng không kể lí do&nbsp;]</span></p>
<p><span style="color:#000000;">Bạn sẽ có 7 ngày để kiểm tra chất lượng sản phẩm, bạn có thể mang giày đi mọi cửa hàng khác để có thể so sánh về chất lượng, so sánh về giá và nếu bạn thấy bên cửa hàng Tu Shoes sai về cam kết chất lượng tốt nhất giá tốt nhất thì bạn cứ mang trả lại cho Tu Shoes.</span></p>
<p><span style="color:#000000;">-&nbsp;<strong>Bảo hành 1 đổi 1 t</strong>rong 3 tháng với lỗi từ nhà sản xuất</span></p>
<p><span style="color:#000000;">Với bất kì lỗi nào từ nhà máy như chất lượng không đảm bảo dẫn đến việc bung keo thì bên mình hỗ trợ nhận lại giày và đổi cho khách hàng sản phẩm mới.</span></p>
<p><span style="color:#000000;">-&nbsp;<strong>Hỗ trợ bảo hành và sửa chữa miễn phí</strong></span></p>
<p><span style="color:#000000;">Với những sản phẩm sau 3 tháng mà gặp lỗi từ giày thì bên shop vẫn sẽ nhận về và hỗ trợ sửa lại giày miễn phí cho khách hàng.</span></p>
<p><span style="color:#000000;"><strong>4. Kênh Thông Tin:</strong></span></p>
<p><span style="color:#000000;">TuShoes có kênh youtube giúp mọi người có thể có cái nhìn tổng quát về chất lượng sản phẩm cũng như việc so sánh các chất lượng khác nhau và kiểm tra chất lượng giày mà mọi người đang sử dụng: </span><u><strong><a href="https://www.youtube.com/channel/UCz-dKtYr02q7FFC3NF5-AQw?view_as=subscriber"><span style="color:#ff0000;">youtube Tu Shoes</span></a></strong></u></p>
<p>&nbsp;</p>
<p><span style="color:#000000;"><strong><u>Bên trên là những kinh nghiệm của Tu Shoes và cam kết của Tu Shoes cũng như hướng dẫn mọi người kinh nghiệm mua sắm và đảm bảo mọi người sẽ mua được sản phẩm đúng với số tiền mà mọi người bỏ ra. Cảm ơn bạn đã xem hết bài viết.</u></strong></span></p>
								</div>
							</div>
						</div>
					</div>', N'https://bizweb.dktcdn.net/100/336/177/articles/6759752f7e408f1ed651.jpg?v=1607336167230', NULL, NULL)
INSERT [dbo].[News] ([ID], [Title], [Content], [Image], [created_at], [updated_at]) VALUES (6, N' Shoes', N'<h1 class="article-title"><a href="/giay-rep-1-1-chuan-gia-re-nhat-tu-shoes" itemprop="name">Giày Rep 1:1 Chuẩn Giá Rẻ Nhất - Tu Shoes</a></h1>
						<div class="post-time">
							Viết bởi <span>Hồ Sỹ Tú</span>, Ngày 07/12/2020
						</div>
						<div class="article-details">
							<div class="article-image">
								<a href="/giay-rep-1-1-chuan-gia-re-nhat-tu-shoes">
									
									<img itemprop="image" class="img-fluid" src="//bizweb.dktcdn.net/100/336/177/articles/6759752f7e408f1ed651.jpg?v=1607336167230" alt="Giày Rep 1:1 Chuẩn Giá Rẻ Nhất - Tu Shoes">
									

								</a>
							</div>							
							<div class="article-content" itemprop="description">
								<div class="rte">
									<p><span style="font-size:18px;"><u><strong><span style="color:#000000;">Vì Sao Giày Chuẩn Rep 1:1 Tu Shoes Lại Rẻ Nhất ?</span></strong></u></span></p>
<p><span style="color:#000000;"><strong>1. Nhập hàng trực tiếp từ nhà máy.</strong></span></p>
<p><span style="color:#000000;">Toàn bộ sản phẩm Tu Shoes đều được nhập trực tiếp từ nhà máy. Không thông qua trung gian cũng như là kiểm tra về chất lượng sản phẩm nghiêm ngặt qua nhiều bước nên Tu Shoes luôn cam kết mang đến chất lượng tốt nhất với giá tốt nhất.</span></p>
<p><span style="color:#000000;"><img data-thumb="original" original-height="1440" original-width="1080" src="//bizweb.dktcdn.net/100/336/177/files/1233211.jpg?v=1607334652468" /><img data-thumb="original" original-height="858" original-width="858" src="//bizweb.dktcdn.net/100/336/177/files/12-6ccd18b5-c060-450c-a1e1-a354ac3c35f8.jpg?v=1607334923653" /><img data-thumb="original" original-height="520" original-width="780" src="//bizweb.dktcdn.net/100/336/177/files/123.jpg?v=1607334932830" /><img data-thumb="original" original-height="1440" original-width="1080" src="//bizweb.dktcdn.net/100/336/177/files/1234.jpg?v=1607334938865" /></span></p>
<p><span style="color:#000000;"><strong>2. Đúng Giá Đúng Chất Lượng [ Nói Không Với Lừa Đảo, Bán Hàng Rác , Trộn Hàng Hoặc Đôn Hàng ]</strong></span></p>
<p><span style="color:#000000;">Mọi người hay so sánh đặt câu hỏi là vì sao cửa hàng này đắt hơn cửa hàng khác 200k, 300k hay thậm chí là 500k ? Đơn giản vì&nbsp;<strong>chất lượng hoàn toàn khác nhau</strong>&nbsp;. Tu Shoes đã làm việc trong ngành giày hơn 3 năm và hiểu rõ điều này. Nếu cửa hàng khác bán chất lượng tương tự mà giá rẻ hơn rất nhiều vậy tại sao Tu Shoes không sang bên cửa hàng đó nhập hết hàng bên đó về và bán giá cao để kiếm lời ? Chỉ đơn giản là&nbsp;<strong>chất lượng hoàn toàn khác nhau cho nên giá tiền cũng hoàn toàn khác nhau&nbsp;</strong>và những cửa hàng bán hàng rẻ tiền quảng cáo chất lượng Rep 1:1 chuẩn là đang&nbsp;<strong>lừa đảo khách hàng.</strong></span></p>
<p>&nbsp;</p>
<p><span style="color:#000000;"><strong>Ví Dụ:&nbsp;</strong>Khách hàng đặt 1 sản phẩm A của Tu Shoes thì shop sẽ mất thời gian khoảng 7-10 ngày để sản phẩm đó có thể vận chuyển từ <b>Nhà Máy&nbsp;</b>về đến shop để giao hàng hóa hay sản phẩm chất lượng cho bạn khách hàng.</span></p>
<p><span style="color:#000000;">Nếu những cửa hàng khác&nbsp;cũng bán hàng rep 1:1 chất lượng tốt mà giá rẻ tại sao bên shop không qua đó lấy hàng chất lượng tốt giá rẻ như quảng cáo để trả hàng cho khách, không phải bắt khách đợi 7-10 ngày nữa&nbsp;?&nbsp;<strong>Chỉ đơn giản là những cửa hàng đó đang lừa đảo khách hàng và hàng hóa đó không đủ tiêu chuẩn Rep 1:1 như đã quảng cáo để mang đến cho khách hàng những chất lượng tốt nhất.</strong></span></p>
<p><span style="color:#000000;"><strong><u>Vì thế đúng giá rẻ nhất và đúng chất lượng tốt nhất chỉ có tại Tu Shoes</u></strong></span></p>
<p>&nbsp;</p>
<p><span style="color:#000000;"><strong>3. Cửa hàng Uy Tín.</strong></span></p>
<p><span style="color:#000000;">&nbsp;Với 1 cửa hàng uy tín thì thực hiện đầy đủ cam kết với khách hàng từ bảo hành hàng hóa, đổi trả hàng hóa cũng như việc nâng cao chất lượng phục vụ là điều quan trọng nhất.</span></p>
<p><span style="color:#000000;">Với TuShoes bạn có thể:</span></p>
<p><span style="color:#000000;">&nbsp;- <b>Đổi Hoặc Trả</b>&nbsp;sản phẩm trong 7 ngày [ Với giày chưa sử dụng không kể lí do&nbsp;]</span></p>
<p><span style="color:#000000;">Bạn sẽ có 7 ngày để kiểm tra chất lượng sản phẩm, bạn có thể mang giày đi mọi cửa hàng khác để có thể so sánh về chất lượng, so sánh về giá và nếu bạn thấy bên cửa hàng Tu Shoes sai về cam kết chất lượng tốt nhất giá tốt nhất thì bạn cứ mang trả lại cho Tu Shoes.</span></p>
<p><span style="color:#000000;">-&nbsp;<strong>Bảo hành 1 đổi 1 t</strong>rong 3 tháng với lỗi từ nhà sản xuất</span></p>
<p><span style="color:#000000;">Với bất kì lỗi nào từ nhà máy như chất lượng không đảm bảo dẫn đến việc bung keo thì bên mình hỗ trợ nhận lại giày và đổi cho khách hàng sản phẩm mới.</span></p>
<p><span style="color:#000000;">-&nbsp;<strong>Hỗ trợ bảo hành và sửa chữa miễn phí</strong></span></p>
<p><span style="color:#000000;">Với những sản phẩm sau 3 tháng mà gặp lỗi từ giày thì bên shop vẫn sẽ nhận về và hỗ trợ sửa lại giày miễn phí cho khách hàng.</span></p>
<p><span style="color:#000000;"><strong>4. Kênh Thông Tin:</strong></span></p>
<p><span style="color:#000000;">TuShoes có kênh youtube giúp mọi người có thể có cái nhìn tổng quát về chất lượng sản phẩm cũng như việc so sánh các chất lượng khác nhau và kiểm tra chất lượng giày mà mọi người đang sử dụng: </span><u><strong><a href="https://www.youtube.com/channel/UCz-dKtYr02q7FFC3NF5-AQw?view_as=subscriber"><span style="color:#ff0000;">youtube Tu Shoes</span></a></strong></u></p>
<p>&nbsp;</p>
<p><span style="color:#000000;"><strong><u>Bên trên là những kinh nghiệm của Tu Shoes và cam kết của Tu Shoes cũng như hướng dẫn mọi người kinh nghiệm mua sắm và đảm bảo mọi người sẽ mua được sản phẩm đúng với số tiền mà mọi người bỏ ra. Cảm ơn bạn đã xem hết bài viết.</u></strong></span></p>
								</div>
							</div>
						</div>
					</div>', N'https://bizweb.dktcdn.net/100/336/177/articles/6759752f7e408f1ed651.jpg?v=1607336167230', NULL, NULL)
INSERT [dbo].[News] ([ID], [Title], [Content], [Image], [created_at], [updated_at]) VALUES (7, N'Giày Rep 1:1 Chuẩn Giá Rẻ Nhất - Tu Shoes', N'<h1 class="article-title"><a href="/giay-rep-1-1-chuan-gia-re-nhat-tu-shoes" itemprop="name">Giày Rep 1:1 Chuẩn Giá Rẻ Nhất - Tu Shoes</a></h1>
						<div class="post-time">
							Viết bởi <span>Hồ Sỹ Tú</span>, Ngày 07/12/2020
						</div>
						<div class="article-details">
							<div class="article-image">
								<a href="/giay-rep-1-1-chuan-gia-re-nhat-tu-shoes">
									
									<img itemprop="image" class="img-fluid" src="//bizweb.dktcdn.net/100/336/177/articles/6759752f7e408f1ed651.jpg?v=1607336167230" alt="Giày Rep 1:1 Chuẩn Giá Rẻ Nhất - Tu Shoes">
									

								</a>
							</div>							
							<div class="article-content" itemprop="description">
								<div class="rte">
									<p><span style="font-size:18px;"><u><strong><span style="color:#000000;">Vì Sao Giày Chuẩn Rep 1:1 Tu Shoes Lại Rẻ Nhất ?</span></strong></u></span></p>
<p><span style="color:#000000;"><strong>1. Nhập hàng trực tiếp từ nhà máy.</strong></span></p>
<p><span style="color:#000000;">Toàn bộ sản phẩm Tu Shoes đều được nhập trực tiếp từ nhà máy. Không thông qua trung gian cũng như là kiểm tra về chất lượng sản phẩm nghiêm ngặt qua nhiều bước nên Tu Shoes luôn cam kết mang đến chất lượng tốt nhất với giá tốt nhất.</span></p>
<p><span style="color:#000000;"><img data-thumb="original" original-height="1440" original-width="1080" src="//bizweb.dktcdn.net/100/336/177/files/1233211.jpg?v=1607334652468" /><img data-thumb="original" original-height="858" original-width="858" src="//bizweb.dktcdn.net/100/336/177/files/12-6ccd18b5-c060-450c-a1e1-a354ac3c35f8.jpg?v=1607334923653" /><img data-thumb="original" original-height="520" original-width="780" src="//bizweb.dktcdn.net/100/336/177/files/123.jpg?v=1607334932830" /><img data-thumb="original" original-height="1440" original-width="1080" src="//bizweb.dktcdn.net/100/336/177/files/1234.jpg?v=1607334938865" /></span></p>
<p><span style="color:#000000;"><strong>2. Đúng Giá Đúng Chất Lượng [ Nói Không Với Lừa Đảo, Bán Hàng Rác , Trộn Hàng Hoặc Đôn Hàng ]</strong></span></p>
<p><span style="color:#000000;">Mọi người hay so sánh đặt câu hỏi là vì sao cửa hàng này đắt hơn cửa hàng khác 200k, 300k hay thậm chí là 500k ? Đơn giản vì&nbsp;<strong>chất lượng hoàn toàn khác nhau</strong>&nbsp;. Tu Shoes đã làm việc trong ngành giày hơn 3 năm và hiểu rõ điều này. Nếu cửa hàng khác bán chất lượng tương tự mà giá rẻ hơn rất nhiều vậy tại sao Tu Shoes không sang bên cửa hàng đó nhập hết hàng bên đó về và bán giá cao để kiếm lời ? Chỉ đơn giản là&nbsp;<strong>chất lượng hoàn toàn khác nhau cho nên giá tiền cũng hoàn toàn khác nhau&nbsp;</strong>và những cửa hàng bán hàng rẻ tiền quảng cáo chất lượng Rep 1:1 chuẩn là đang&nbsp;<strong>lừa đảo khách hàng.</strong></span></p>
<p>&nbsp;</p>
<p><span style="color:#000000;"><strong>Ví Dụ:&nbsp;</strong>Khách hàng đặt 1 sản phẩm A của Tu Shoes thì shop sẽ mất thời gian khoảng 7-10 ngày để sản phẩm đó có thể vận chuyển từ <b>Nhà Máy&nbsp;</b>về đến shop để giao hàng hóa hay sản phẩm chất lượng cho bạn khách hàng.</span></p>
<p><span style="color:#000000;">Nếu những cửa hàng khác&nbsp;cũng bán hàng rep 1:1 chất lượng tốt mà giá rẻ tại sao bên shop không qua đó lấy hàng chất lượng tốt giá rẻ như quảng cáo để trả hàng cho khách, không phải bắt khách đợi 7-10 ngày nữa&nbsp;?&nbsp;<strong>Chỉ đơn giản là những cửa hàng đó đang lừa đảo khách hàng và hàng hóa đó không đủ tiêu chuẩn Rep 1:1 như đã quảng cáo để mang đến cho khách hàng những chất lượng tốt nhất.</strong></span></p>
<p><span style="color:#000000;"><strong><u>Vì thế đúng giá rẻ nhất và đúng chất lượng tốt nhất chỉ có tại Tu Shoes</u></strong></span></p>
<p>&nbsp;</p>
<p><span style="color:#000000;"><strong>3. Cửa hàng Uy Tín.</strong></span></p>
<p><span style="color:#000000;">&nbsp;Với 1 cửa hàng uy tín thì thực hiện đầy đủ cam kết với khách hàng từ bảo hành hàng hóa, đổi trả hàng hóa cũng như việc nâng cao chất lượng phục vụ là điều quan trọng nhất.</span></p>
<p><span style="color:#000000;">Với TuShoes bạn có thể:</span></p>
<p><span style="color:#000000;">&nbsp;- <b>Đổi Hoặc Trả</b>&nbsp;sản phẩm trong 7 ngày [ Với giày chưa sử dụng không kể lí do&nbsp;]</span></p>
<p><span style="color:#000000;">Bạn sẽ có 7 ngày để kiểm tra chất lượng sản phẩm, bạn có thể mang giày đi mọi cửa hàng khác để có thể so sánh về chất lượng, so sánh về giá và nếu bạn thấy bên cửa hàng Tu Shoes sai về cam kết chất lượng tốt nhất giá tốt nhất thì bạn cứ mang trả lại cho Tu Shoes.</span></p>
<p><span style="color:#000000;">-&nbsp;<strong>Bảo hành 1 đổi 1 t</strong>rong 3 tháng với lỗi từ nhà sản xuất</span></p>
<p><span style="color:#000000;">Với bất kì lỗi nào từ nhà máy như chất lượng không đảm bảo dẫn đến việc bung keo thì bên mình hỗ trợ nhận lại giày và đổi cho khách hàng sản phẩm mới.</span></p>
<p><span style="color:#000000;">-&nbsp;<strong>Hỗ trợ bảo hành và sửa chữa miễn phí</strong></span></p>
<p><span style="color:#000000;">Với những sản phẩm sau 3 tháng mà gặp lỗi từ giày thì bên shop vẫn sẽ nhận về và hỗ trợ sửa lại giày miễn phí cho khách hàng.</span></p>
<p><span style="color:#000000;"><strong>4. Kênh Thông Tin:</strong></span></p>
<p><span style="color:#000000;">TuShoes có kênh youtube giúp mọi người có thể có cái nhìn tổng quát về chất lượng sản phẩm cũng như việc so sánh các chất lượng khác nhau và kiểm tra chất lượng giày mà mọi người đang sử dụng: </span><u><strong><a href="https://www.youtube.com/channel/UCz-dKtYr02q7FFC3NF5-AQw?view_as=subscriber"><span style="color:#ff0000;">youtube Tu Shoes</span></a></strong></u></p>
<p>&nbsp;</p>
<p><span style="color:#000000;"><strong><u>Bên trên là những kinh nghiệm của Tu Shoes và cam kết của Tu Shoes cũng như hướng dẫn mọi người kinh nghiệm mua sắm và đảm bảo mọi người sẽ mua được sản phẩm đúng với số tiền mà mọi người bỏ ra. Cảm ơn bạn đã xem hết bài viết.</u></strong></span></p>
								</div>
							</div>
						</div>', N'https://bizweb.dktcdn.net/100/336/177/articles/6759752f7e408f1ed651.jpg?v=1607336167230', NULL, NULL)
SET IDENTITY_INSERT [dbo].[News] OFF
GO
SET IDENTITY_INSERT [dbo].[Product] ON 

INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (1, N'AIR TAILWIND 79 SE BLACK', 1, N'Giày chất lượng cao', 1300000.0000, 1040000.0000, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/6-dad445b8-9d65-4381-a564-02429370df11.jpg?v=1616832056713', N'1 đổi 1', CAST(N'2021-04-01T10:07:16.700' AS DateTime), CAST(N'2021-04-01T10:15:12.877' AS DateTime))
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (8, N'AIR FORCE 1 LV8 KSA GS ''3D GLASSES''', 1, N'Giày chất lượng cao', 1600000.0000, 1280000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/2-6b384707-f3fe-4b0a-8c06-5e0cd7b104d6.jpg?v=1616479119830', N'1 đổi 1', CAST(N'2021-04-01T10:15:50.813' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (9, N'SEAN CLIVER X DUNK LOW SB ''HOLIDAY SPECIAL''', 1, N'Giày chất lượng cao', 2700000.0000, 2160000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/24-bf8ef4b9-f1e3-4b7f-9485-2fb9a7bc091c.jpg?v=1616479016240', N'1 đổi 1', CAST(N'2021-04-01T10:16:29.693' AS DateTime), CAST(N'2021-04-01T10:16:47.697' AS DateTime))
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (10, N'AIR JORDAN 1 RETRO HIGH OG ''VOLT GOLD''', 1, N'Giày chất lượng cao', 2200000.0000, 1760000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/12-b486e5d2-7d09-4947-838e-62d428bb6716.jpg?v=1616478916000', N'1 đổi 1', CAST(N'2021-04-01T10:17:26.093' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (11, N'AIR JORDAN 1 RETRO HIGH ''85 ''VARSITY RED''', 1, N'Giày chất lượng cao', 3500000.0000, 2800000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/27-3e2e1f85-449a-46f8-85ea-72ddd361145e.jpg?v=1611559414000', N'1 đổi 1', CAST(N'2021-04-01T10:17:57.113' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (12, N'STUSSY X AIR FORCE 1 LOW ''FOSSIL''', 1, N'Giày chất lượng cao', 3200000.0000, 2560000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/9-5404bf6a-5b82-4dd4-bb96-40fe73cc8ad0.jpg?v=1616478847267', N'1 đổi 1', CAST(N'2021-04-01T10:18:29.697' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (13, N'AIR JORDAN 1 MID ''CHICAGO''', 1, N'Giày chất lượng cao', 2000000.0000, 1600000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/34-3f5ef5bc-7704-44a9-825a-b7557469bcdf.jpg?v=1611559538610', N'1 đổi 1', CAST(N'2021-04-01T10:18:58.817' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (14, N'AIR JORDAN 1 RETRO HIGH OG ''TOP 3''', 1, N'Giày chất lượng cao', 3100000.0000, 2480000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/23-3d7580df-1cf8-410e-9617-b2f741417a99.jpg?v=1611559594283', N'1 đổi 1', CAST(N'2021-04-01T10:20:00.133' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (15, N'WMNS AIR FORCE 1 SHADOW ''PALE IVORY''', 1, N'Giày chất lượng cao', 1900000.0000, 1520000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/2-d24925a1-82c5-4fa6-a397-149c7050eb41.jpg?v=1610525299000', N'1 đổi 1', CAST(N'2021-04-01T10:20:31.543' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (16, N'AIR JORDAN 1 MID ''OBSIDIAN''', 1, N'Giày chất lượng cao', 2000000.0000, 1600000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/8-7584c7a0-0b3a-4ce6-9d18-c323bbfee832.jpg?v=1608195358000', N'1 đổi 1', CAST(N'2021-04-01T10:21:02.710' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (17, N'AIR JORDAN 1 RETRO HIGH OG ''UNC''', 1, N'Giày chất lượng cao', 2500000.0000, 2000000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/68.jpg?v=1606282794633', N'1 đổi 1', CAST(N'2021-04-01T10:21:27.307' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (18, N'AIR JORDAN 1 RETRO HIGH OG GS ''TURBO GREEN''', 1, N'Giày chất lượng cao', 2200000.0000, 1760000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/32.jpg?v=1605765077000', N'1 đổi 1', CAST(N'2021-04-01T10:21:57.413' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (19, N'SUPREME X AIR FORCE 1 LOW', 1, N'Giày chất lượng cao', 1900000.0000, 1520000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf6600.jpg?v=1592394665917', N'1 đổi 1', CAST(N'2021-04-01T10:22:38.347' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (20, N'Air Max 720 Green', 1, N'Giày chất lượng cao', 1600000.0000, 800000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf2921.jpg?v=1562751355207', N'1 đổi 1', CAST(N'2021-04-01T10:23:12.927' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (21, N'Air Max 97 Black', 1, N'Giày chất lượng cao', 1500000.0000, 750000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/12-80466ec9-118e-4db8-8596-8ac5c12c09c2.jpg?v=1546919912917', N'1 đổi 1', CAST(N'2021-04-01T10:23:47.230' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (22, N'M2K Tekno', 1, N'Giày chất lượng cao', 1500000.0000, 1200000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/7.jpg?v=1586864907913', N'1 đổi 1', CAST(N'2021-04-01T10:24:46.677' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (23, N'FRAGMENT DESIGN X AIR JORDAN 3 RETRO SP ''WHITE''', 1, N'Giày chất lượng cao', 3600000.0000, 2880000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/2-831fcf72-a027-4602-b7a7-072af82c2dd4.jpg?v=1611560048870', N'1 đổi 1', CAST(N'2021-04-01T10:25:25.477' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (24, N'OFF-WHITE X WMNS AIR JORDAN 4 SP ''SAIL''', 1, N'Giày chất lượng cao', 3900000.0000, 3120000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/9-425d7ca9-7e1a-4551-b659-1718e4b88b15.jpg?v=1611559971000', N'1 đổi 1', CAST(N'2021-04-01T10:26:09.227' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (25, N'AIR FORCE 1 SHADOW ''HYDROGEN BLUE''', 1, N'Giày chất lượng cao', 1900000.0000, 1520000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/2-1-5d469925-98d1-4cb7-82e8-51e7dbdad278.jpg?v=1610525332037', N'1 đổi 1', CAST(N'2021-04-01T10:26:40.717' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (26, N'DUNK LOW PRO SB ''HEINEKEN''', 1, N'Giày chất lượng cao', 3800000.0000, 3050000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/2-48d0d16d-e578-47ce-85cc-a7be727ee021.jpg?v=1602405505600', N'1 đổi 1', CAST(N'2021-04-01T10:35:58.697' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (27, N'Stan Smith Appears With A New Tri-Color Heel Tab', 2, N'Giày chất lượng cao', 1100000.0000, 880000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf9712.jpg?v=1604392720600', N'1 đổi 1', CAST(N'2021-04-01T11:09:35.213' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (28, N'Yeezy Boost 350 V2 Zyon - 1:1', 2, N'Giày chất lượng cao', 2000000.0000, 1600000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf6694.jpg?v=1592988687157', N'1 đổi 1', CAST(N'2021-04-01T11:10:05.437' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (29, N'Yeezy Boost 350 V2 linen - 1:1', 2, N'Giày chất lượng cao', 2000000.0000, 1600000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf6606.jpg?v=1592394853213', N'1 đổi 1', CAST(N'2021-04-01T11:10:31.017' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (30, N'YEEZY BOOST 380 MIST REFLECTIVE', 2, N'Giày chất lượng cao', 2300000.0000, 1600000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/16-6f1033e3-1838-40d7-9e86-04a3c0038498.jpg?v=1586864978850', N'1 đổi 1', CAST(N'2021-04-01T11:11:04.230' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (31, N'YEEZY BOOST 700 ANALOG - REP 1:1', 2, N'Giày chất lượng cao', 3300000.0000, 1650000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf3580.jpg?v=1565424145477', N'1 đổi 1', CAST(N'2021-04-01T11:11:41.130' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (32, N'PROPHERE "BLACK WHITE" - REP 1:1', 2, N'Giày chất lượng cao', 1100000.0000, 550000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf3096.jpg?v=1562840806277', N'1 đổi 1', CAST(N'2021-04-01T11:12:45.317' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (33, N'Stan Smith Hologram', 2, N'Giày chất lượng cao', 1050000.0000, 850000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/87.jpg?v=1547837364243', N'1 đổi 1', CAST(N'2021-04-01T11:13:19.653' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (34, N'YEEZY 350 V2 BOOST ZEBRA', 2, N'Giày chất lượng cao', 1600000.0000, 1200000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/800502-02.jpg?v=1546919919000', N'1 đổi 1', CAST(N'2021-04-01T11:13:59.163' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (35, N'Stan Smith Green', 2, N'Giày chất lượng cao', 1050000.0000, 840000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/50003341-2365870303644467-7038016969261449216-o.jpg?v=1547535181000', N'1 đổi 1', CAST(N'2021-04-01T11:15:03.160' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (36, N'Stan Smith White', 2, N'Giày chất lượng cao', 1050000.0000, 840000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/22-0794bee4-5e0e-4a3e-a27f-89e14503e197.jpg?v=1547361689000', N'1 đổi 1', CAST(N'2021-04-01T11:15:27.327' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (37, N'Track Mule', 3, N'Giày chất lượng cao', 3500000.0000, 2800000.0000, N'https://balenciaga.dam.kering.com/m/6cbd5d97c23093e7/Medium-653814W3CP39000_F.jpg?v=4', N'1 đổi 1', CAST(N'2021-04-01T11:17:46.653' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (38, N'Track Mule Black', 3, N'Giày chất lượng cao', 3500000.0000, 2800000.0000, N'https://balenciaga.dam.kering.com/m/41fe2859ea05b5df/Medium-653814W3CP31000_F.jpg?v=4', N'1 đổi 1', CAST(N'2021-04-01T11:18:49.347' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (39, N'Track Hike', 3, N'Giày chất lượng cao', 4000000.0000, 3200000.0000, N'https://balenciaga.dam.kering.com/m/5205767185bd2195/Medium-654867W3CP31000_F.jpg?v=4', N'1 đổi 1', CAST(N'2021-04-01T11:19:42.600' AS DateTime), CAST(N'2021-04-01T11:20:15.827' AS DateTime))
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (40, N'Trip S White', 3, N'Giày chất lượng cao', 1900000.0000, 1280000.0000, N'https://balenciaga.dam.kering.com/m/5e158bc21375bce1/Large-536737W2FW19700_F.jpg', N'1 đổi 1', CAST(N'2021-04-01T11:21:16.780' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (41, N'Trip S Black', 3, N'Giày chất lượng cao', 1900000.0000, 1280000.0000, N'https://balenciaga.dam.kering.com/m/2df4728f55afac78/Large-536737W2FA11090_F.jpg', N'1 đổi 1', CAST(N'2021-04-01T11:22:35.223' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (42, N'MEN''S SPEED CLEAR SOLE SNEAKER IN BLACK', 3, N'Giày chất lượng cao', 2000000.0000, 1600000.0000, N'https://balenciaga.dam.kering.com/m/312a3e2ae622d09f/Large-607544W2DBL1000_F.jpg?v=3', N'1 đổi 1', CAST(N'2021-04-01T11:24:01.443' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (48, N'GUCCI X DISNEY 2020', 4, N'Giày chất lượng cao', 2600000.0000, 2080000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf6722.jpg?v=1592988116000', N'1 đổi 1', CAST(N'2021-04-01T11:26:57.630' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (49, N'Gucci Tiger', 4, N'Giày chất lượng cao', 2500000.0000, 1500000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf2975.jpg?v=1562751236723', N'1 đổi 1', CAST(N'2021-04-01T11:27:36.100' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (50, N'Gucci Rhyton sneaker with mouth print', 4, N'Giày chất lượng cao', 2900000.0000, 1450000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf2964.jpg?v=1562751313383', N'1 đổi 1', CAST(N'2021-04-01T11:28:15.067' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (51, N'Gucci Snake', 4, N'Giày chất lượng cao', 2500000.0000, 1500000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf2988.jpg?v=1563162475013', N'1 đổi 1', CAST(N'2021-04-01T11:28:39.630' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (52, N'Gucci Bee', 4, N'Giày chất lượng cao', 2500000.0000, 1500000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf2981.jpg?v=1563162535740', N'1 đổi 1', CAST(N'2021-04-01T11:29:02.877' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (53, N'Vans Vault Style 36 Black', 5, N'Giày chất lượng cao', 1200000.0000, 960000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf4671.jpg?v=1575438868000', N'1 đổi 1', CAST(N'2021-04-01T11:29:41.490' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (54, N'Vans Era Checkerboard 2020', 5, N'Giày chất lượng cao', 900000.0000, 720000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf9057.jpg?v=1598952563603', N'1 đổi 1', CAST(N'2021-04-01T11:30:10.473' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (55, N'VANS VAULT CHECKERBOARD 2020', 5, N'Giày chất lượng cao', 900000.0000, 720000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf9368.jpg?v=1602243562447', N'1 đổi 1', CAST(N'2021-04-01T11:30:33.477' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (56, N'Vans Old Skool Classic Navy', 5, N'Giày chất lượng cao', 900000.0000, 720000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf3850.jpg?v=1570871052020', N'1 đổi 1', CAST(N'2021-04-01T11:31:09.717' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (57, N'Vans Vault Style 36 Suede/Canvas', 5, N'Giày chất lượng cao', 900000.0000, 720000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf3845.jpg?v=1570870922023', N'1 đổi 1', CAST(N'2021-04-01T11:31:34.750' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (58, N'Vans Style 36 Navy', 5, N'Giày chất lượng cao', 950000.0000, 660000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/114.jpg?v=1547967272397', N'1 đổi 1', CAST(N'2021-04-01T11:32:04.073' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (59, N'Vans Style 36 Green', 5, N'Giày chất lượng cao', 950000.0000, 570000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/103.jpg?v=1547967343350', N'1 đổi 1', CAST(N'2021-04-01T11:32:27.727' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (62, N'Vans Style 36 Red', 5, N'Giày chất lượng cao', 950000.0000, 760000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/88-40092ec2-9c6a-4867-8a70-ba056fd33d8f.jpg?v=1547967316000', N'1 đổi 1', CAST(N'2021-04-01T11:33:12.063' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (63, N'Vans Style 36 Orange', 5, N'Giày chất lượng cao', 990000.0000, 594000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf3651-79a63de2-99a6-423b-80f2-b8634848f4f2.jpg?v=1566628717663', N'1 đổi 1', CAST(N'2021-04-01T11:33:45.927' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (64, N'Fear of God x Chuck 70 Hi ''Grey''', 6, N'Giày chất lượng cao', 1400000.0000, 1120000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf6679.jpg?v=1592988533490', N'1 đổi 1', CAST(N'2021-04-01T11:34:16.190' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (65, N'1970s White Low', 6, N'Giày chất lượng cao', 1200000.0000, 840000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/46425094-2327006747530823-1323242581668134912-o.jpg?v=1546919915250', N'1 đổi 1', CAST(N'2021-04-01T11:34:43.830' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (66, N'1970s Black High', 6, N'Giày chất lượng cao', 1200000.0000, 840000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/46420111-2327007577530740-6101184310222520320-o.jpg?v=1546919915000', N'1 đổi 1', CAST(N'2021-04-01T11:35:07.227' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (67, N'Chuck 70s x CDG White High', 6, N'Giày chất lượng cao', 1600000.0000, 920000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/17.jpg?v=1546919922130', N'1 đổi 1', CAST(N'2021-04-01T11:35:33.013' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (68, N'Chuck 70s x CDG White Low', 6, N'Giày chất lượng cao', 1200000.0000, 840000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/12.jpg?v=1546919922000', N'1 đổi 1', CAST(N'2021-04-01T11:35:59.377' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (69, N'1970s Sunflowers', 6, N'Giày chất lượng cao', 1200000.0000, 720000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/42669430-2297178037180361-6679355885438894080-o-2297178033847028.jpg?v=1546919922333', N'1 đổi 1', CAST(N'2021-04-01T11:36:20.923' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (72, N'MLB BIG BALL CHUNKY SHINY NEW YORK YANKEES', 7, N'Giày chất lượng cao', 990000.0000, 792000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf9239.jpg?v=1611741896000', N'1 đổi 1', CAST(N'2021-04-01T11:37:48.293' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (73, N'MLB NY Cream', 7, N'Giày chất lượng cao', 990000.0000, 792000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf4560.jpg?v=1573540617000', N'1 đổi 1', CAST(N'2021-04-01T11:38:13.457' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (74, N'MLB Boston', 7, N'Giày chất lượng cao', 990000.0000, 792000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf4570.jpg?v=1573540760000', N'1 đổi 1', CAST(N'2021-04-01T11:38:36.587' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (75, N'MLB Big Ball Chunky a NY', 7, N'Giày chất lượng cao', 990000.0000, 792000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf6561.jpg?v=1592120820000', N'1 đổi 1', CAST(N'2021-04-01T11:39:05.280' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (76, N'MLB CHUNKY MONOGRAM LT NEW YORK', 7, N'Giày chất lượng cao', 990000.0000, 720000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf9689.jpg?v=1604392887000', N'1 đổi 1', CAST(N'2021-04-01T11:39:32.977' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (77, N'MLB BIG BALL CHUNKY MICKEY NY - CREAM', 7, N'Giày chất lượng cao', 990000.0000, 720000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf9695.jpg?v=1604393033690', N'1 đổi 1', CAST(N'2021-04-01T11:40:20.263' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (78, N'MLB LA', 7, N'Giày chất lượng cao', 950000.0000, 720000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf4575.jpg?v=1573540721977', N'1 đổi 1', CAST(N'2021-04-01T11:40:46.100' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (79, N'MLB NY Black', 7, N'Giày chất lượng cao', 950000.0000, 720000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf4586.jpg?v=1573540652670', N'1 đổi 1', CAST(N'2021-04-01T11:41:16.663' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (80, N'New Balance 327 Blue Orange', 10, N'Giày chất lượng cao', 1200000.0000, 960000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf6656.jpg?v=1599816561337', N'1 đổi 1', CAST(N'2021-04-01T11:41:46.553' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (81, N'New Balance 327 Pride (2020)', 10, N'Giày chất lượng cao', 1200000.0000, 960000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf6674.jpg?v=1592988245783', N'1 đổi 1', CAST(N'2021-04-01T11:42:20.810' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (82, N'New Balance 327 Black (W)', 10, N'Giày chất lượng cao', 1200000.0000, 960000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf6661.jpg?v=1592988269917', N'1 đổi 1', CAST(N'2021-04-01T11:42:44.437' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (83, N'New Balance 327 Casablanca Orange', 10, N'Giày chất lượng cao', 1400000.0000, 1120000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf6669.jpg?v=1592988336397', N'1 đổi 1', CAST(N'2021-04-01T11:43:22.483' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (84, N'New Balance 327 Casablanca Green', 10, N'Giày chất lượng cao', 1400000.0000, 1120000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf6650.jpg?v=1592988356000', N'1 đổi 1', CAST(N'2021-04-01T11:43:50.810' AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[Product] OFF
GO
SET IDENTITY_INSERT [dbo].[Product_Color] ON 

INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (1, 1, 1, 21, CAST(N'2021-04-02T07:54:00.097' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (2, 3, 11, 20, CAST(N'2021-04-02T07:54:48.653' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (3, 2, 8, 20, CAST(N'2021-04-02T07:55:23.417' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (4, 5, 9, 20, CAST(N'2021-04-02T07:56:03.107' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (5, 4, 10, 20, CAST(N'2021-04-02T07:56:14.077' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (6, 6, 12, 20, CAST(N'2021-04-02T07:56:47.463' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (7, 3, 13, 20, CAST(N'2021-04-02T07:57:35.773' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (8, 5, 14, 20, CAST(N'2021-04-02T07:58:10.507' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (9, 2, 15, 20, CAST(N'2021-04-02T07:58:49.177' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (10, 1, 16, 20, CAST(N'2021-04-02T07:59:10.133' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (11, 5, 17, 20, CAST(N'2021-04-02T07:59:49.603' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (12, 5, 18, 20, CAST(N'2021-04-02T07:59:58.137' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (13, 2, 19, 20, CAST(N'2021-04-02T08:00:25.627' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (14, 5, 20, 20, CAST(N'2021-04-02T08:00:32.750' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (15, 1, 21, 20, CAST(N'2021-04-02T08:00:39.450' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (16, 2, 22, 20, CAST(N'2021-04-02T08:00:52.773' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (17, 2, 23, 20, CAST(N'2021-04-02T08:00:59.650' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (18, 2, 24, 20, CAST(N'2021-04-02T08:01:22.857' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (19, 5, 25, 20, CAST(N'2021-04-02T08:01:34.003' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (20, 5, 26, 20, CAST(N'2021-04-02T08:01:41.467' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (21, 2, 27, 20, CAST(N'2021-04-02T08:01:57.447' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (22, 6, 28, 20, CAST(N'2021-04-02T08:02:12.173' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (23, 4, 29, 20, CAST(N'2021-04-02T08:02:28.337' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (24, 6, 30, 20, CAST(N'2021-04-02T08:02:57.197' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (25, 6, 31, 20, CAST(N'2021-04-02T08:03:30.037' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (26, 1, 32, 20, CAST(N'2021-04-02T08:03:55.613' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (27, 2, 33, 20, CAST(N'2021-04-02T08:04:09.617' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (28, 6, 34, 20, CAST(N'2021-04-02T08:04:26.063' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (29, 5, 35, 20, CAST(N'2021-04-02T08:04:32.960' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (30, 2, 36, 20, CAST(N'2021-04-02T08:04:38.500' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (31, 2, 37, 20, CAST(N'2021-04-02T08:05:06.470' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (32, 1, 38, 20, CAST(N'2021-04-02T08:05:14.860' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (33, 1, 39, 20, CAST(N'2021-04-02T08:05:46.137' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (34, 2, 40, 20, CAST(N'2021-04-02T08:05:51.430' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (35, 1, 41, 20, CAST(N'2021-04-02T08:05:57.817' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (36, 1, 42, 20, CAST(N'2021-04-02T08:06:03.520' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (37, 2, 48, 20, CAST(N'2021-04-02T08:06:26.877' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (38, 2, 49, 20, CAST(N'2021-04-02T08:06:36.660' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (39, 2, 50, 20, CAST(N'2021-04-02T08:06:52.243' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (40, 2, 51, 20, CAST(N'2021-04-02T08:07:02.927' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (41, 2, 52, 20, CAST(N'2021-04-02T08:07:09.290' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (42, 1, 53, 20, CAST(N'2021-04-02T08:07:14.733' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (43, 6, 54, 20, CAST(N'2021-04-02T08:07:27.967' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (44, 6, 55, 20, CAST(N'2021-04-02T08:07:44.320' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (45, 4, 56, 20, CAST(N'2021-04-02T08:07:58.030' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (46, 5, 57, 20, CAST(N'2021-04-02T08:08:16.093' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (47, 4, 58, 20, CAST(N'2021-04-02T08:09:24.930' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (48, 5, 59, 20, CAST(N'2021-04-02T08:09:30.953' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (49, 3, 62, 20, CAST(N'2021-04-02T08:09:40.580' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (50, 7, 63, 20, CAST(N'2021-04-02T08:09:54.987' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (51, 6, 64, 20, CAST(N'2021-04-02T08:10:01.497' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (52, 2, 65, 20, CAST(N'2021-04-02T08:10:08.780' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (53, 1, 66, 20, CAST(N'2021-04-02T08:10:15.193' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (54, 2, 67, 20, CAST(N'2021-04-02T08:10:20.970' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (55, 2, 68, 20, CAST(N'2021-04-02T08:10:30.123' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (56, 7, 69, 20, CAST(N'2021-04-02T08:10:51.587' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (57, 2, 72, 20, CAST(N'2021-04-02T08:11:49.743' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (58, 2, 73, 20, CAST(N'2021-04-02T08:12:06.393' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (59, 2, 74, 20, CAST(N'2021-04-02T08:12:12.633' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (60, 2, 75, 20, CAST(N'2021-04-02T08:12:18.727' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (61, 2, 76, 20, CAST(N'2021-04-02T08:12:25.033' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (62, 2, 77, 20, CAST(N'2021-04-02T08:12:36.177' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (63, 2, 78, 5, CAST(N'2021-04-02T08:12:45.743' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (64, 1, 79, 20, CAST(N'2021-04-02T08:12:51.240' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (65, 7, 80, 5, CAST(N'2021-04-02T08:13:08.823' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (66, 1, 82, 20, CAST(N'2021-04-02T08:13:42.860' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (67, 7, 83, 2, CAST(N'2021-04-02T08:13:57.667' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (68, 5, 84, 20, CAST(N'2021-04-02T08:14:04.403' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (71, 1, 1, 20, CAST(N'2021-04-05T09:59:46.230' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (72, 1, 1, 20, CAST(N'2021-04-05T10:06:26.507' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (73, 2, 8, 20, CAST(N'2021-04-05T10:10:26.797' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (74, 2, 8, 20, CAST(N'2021-04-05T10:10:34.017' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (75, 5, 9, 20, CAST(N'2021-04-05T10:13:07.600' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (76, 5, 9, 20, CAST(N'2021-04-05T10:13:14.667' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (77, 4, 10, 20, CAST(N'2021-04-05T10:13:27.530' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (78, 4, 10, 20, CAST(N'2021-04-05T10:13:35.143' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (79, 3, 11, 20, CAST(N'2021-04-05T10:13:51.653' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (80, 3, 11, 20, CAST(N'2021-04-05T10:13:58.770' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (81, 6, 12, 20, CAST(N'2021-04-05T10:14:19.633' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (82, 6, 12, 20, CAST(N'2021-04-05T10:14:25.823' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (83, 3, 13, 20, CAST(N'2021-04-05T10:15:09.010' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (84, 3, 13, 20, CAST(N'2021-04-05T10:15:15.043' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (85, 5, 14, 20, CAST(N'2021-04-05T10:16:00.943' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (87, 5, 14, 20, CAST(N'2021-04-05T10:16:57.017' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (88, 2, 15, 20, CAST(N'2021-04-05T10:17:17.667' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (89, 2, 15, 20, CAST(N'2021-04-05T10:17:24.047' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (90, 1, 16, 20, CAST(N'2021-04-05T10:17:38.140' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (91, 1, 16, 20, CAST(N'2021-04-05T10:17:44.490' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (92, 5, 17, 20, CAST(N'2021-04-05T10:18:10.163' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (93, 5, 17, 20, CAST(N'2021-04-05T10:18:16.947' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (94, 5, 18, 20, CAST(N'2021-04-05T10:18:39.543' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (95, 5, 18, 20, CAST(N'2021-04-05T10:18:47.850' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (96, 2, 19, 20, CAST(N'2021-04-05T10:19:30.903' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (97, 2, 19, 20, CAST(N'2021-04-05T10:19:38.867' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (98, 5, 20, 20, CAST(N'2021-04-05T10:20:09.743' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (99, 5, 20, 20, CAST(N'2021-04-05T10:20:15.967' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (100, 1, 21, 20, CAST(N'2021-04-05T10:20:30.827' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (101, 1, 21, 20, CAST(N'2021-04-05T10:20:35.307' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (102, 2, 22, 20, CAST(N'2021-04-05T10:21:50.050' AS DateTime), NULL, N'40')
GO
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (103, 2, 22, 20, CAST(N'2021-04-05T10:21:55.110' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (104, 2, 23, 20, CAST(N'2021-04-05T10:22:06.943' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (105, 2, 23, 20, CAST(N'2021-04-05T10:22:11.977' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (106, 2, 24, 20, CAST(N'2021-04-05T10:22:43.417' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (107, 2, 24, 20, CAST(N'2021-04-05T10:22:50.680' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (108, 5, 25, 20, CAST(N'2021-04-05T10:23:13.140' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (109, 5, 25, 20, CAST(N'2021-04-05T10:23:20.260' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (110, 5, 26, 20, CAST(N'2021-04-05T10:23:33.643' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (111, 5, 26, 20, CAST(N'2021-04-05T10:23:40.770' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (112, 2, 27, 20, CAST(N'2021-04-05T10:24:15.023' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (113, 2, 27, 20, CAST(N'2021-04-05T10:24:22.520' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (114, 6, 28, 20, CAST(N'2021-04-05T10:24:35.170' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (115, 6, 28, 20, CAST(N'2021-04-05T10:24:41.800' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (116, 4, 29, 20, CAST(N'2021-04-05T10:25:00.187' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (117, 4, 29, 20, CAST(N'2021-04-05T10:25:11.843' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (118, 6, 30, 20, CAST(N'2021-04-05T10:25:23.287' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (119, 6, 30, 20, CAST(N'2021-04-05T10:25:33.360' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (120, 6, 31, 20, CAST(N'2021-04-05T10:25:41.823' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (121, 6, 31, 20, CAST(N'2021-04-05T10:25:55.140' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (122, 1, 32, 20, CAST(N'2021-04-05T10:26:23.470' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (123, 1, 32, 20, CAST(N'2021-04-05T10:26:30.753' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (124, 2, 33, 20, CAST(N'2021-04-05T10:26:39.923' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (125, 2, 33, 20, CAST(N'2021-04-05T10:26:46.527' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (126, 6, 34, 20, CAST(N'2021-04-05T10:27:04.923' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (127, 6, 34, 20, CAST(N'2021-04-05T10:27:20.270' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (128, 5, 35, 20, CAST(N'2021-04-05T10:27:37.937' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (129, 5, 35, 20, CAST(N'2021-04-05T10:27:46.513' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (130, 2, 36, 20, CAST(N'2021-04-05T10:27:57.017' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (131, 2, 36, 20, CAST(N'2021-04-05T10:28:06.853' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (132, 2, 37, 20, CAST(N'2021-04-05T10:29:18.507' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (133, 2, 37, 20, CAST(N'2021-04-05T10:29:24.637' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (134, 1, 38, 20, CAST(N'2021-04-05T10:29:37.363' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (135, 1, 38, 20, CAST(N'2021-04-05T10:29:44.260' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (136, 1, 39, 20, CAST(N'2021-04-05T10:30:02.883' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (137, 1, 39, 20, CAST(N'2021-04-05T10:30:08.820' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (138, 2, 40, 20, CAST(N'2021-04-05T10:30:24.230' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (139, 2, 40, 20, CAST(N'2021-04-05T10:30:29.357' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (140, 1, 41, 20, CAST(N'2021-04-05T10:30:45.597' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (141, 1, 41, 20, CAST(N'2021-04-05T10:30:50.487' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (142, 1, 42, 20, CAST(N'2021-04-05T10:30:59.303' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (143, 1, 42, 20, CAST(N'2021-04-05T10:31:04.867' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (144, 2, 48, 20, CAST(N'2021-04-05T10:31:34.293' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (145, 2, 48, 20, CAST(N'2021-04-05T10:31:38.980' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (146, 2, 49, 20, CAST(N'2021-04-05T10:31:44.673' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (147, 2, 49, 20, CAST(N'2021-04-05T10:31:49.013' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (148, 2, 50, 20, CAST(N'2021-04-05T10:31:54.467' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (149, 2, 50, 20, CAST(N'2021-04-05T10:31:59.537' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (150, 2, 51, 20, CAST(N'2021-04-05T10:32:05.900' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (151, 2, 51, 20, CAST(N'2021-04-05T10:32:10.790' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (152, 2, 52, 20, CAST(N'2021-04-05T10:32:16.517' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (153, 2, 52, 20, CAST(N'2021-04-05T10:32:22.827' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (154, 1, 53, 20, CAST(N'2021-04-05T10:32:30.600' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (155, 1, 53, 20, CAST(N'2021-04-05T10:32:35.277' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (156, 6, 54, 20, CAST(N'2021-04-05T10:32:55.383' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (157, 6, 54, 20, CAST(N'2021-04-05T10:33:00.167' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (158, 6, 55, 20, CAST(N'2021-04-05T10:33:07.793' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (159, 6, 55, 20, CAST(N'2021-04-05T10:33:19.387' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (160, 4, 56, 20, CAST(N'2021-04-05T10:33:26.917' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (161, 4, 56, 20, CAST(N'2021-04-05T10:33:31.347' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (162, 5, 57, 20, CAST(N'2021-04-05T10:33:39.343' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (163, 5, 57, 20, CAST(N'2021-04-05T10:33:44.037' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (164, 4, 58, 20, CAST(N'2021-04-05T10:33:51.753' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (165, 4, 58, 20, CAST(N'2021-04-05T10:33:56.653' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (166, 5, 59, 20, CAST(N'2021-04-05T10:34:03.997' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (167, 5, 59, 20, CAST(N'2021-04-05T10:34:09.207' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (168, 3, 62, 20, CAST(N'2021-04-05T10:34:38.170' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (169, 3, 62, 20, CAST(N'2021-04-05T10:34:43.143' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (170, 7, 63, 20, CAST(N'2021-04-05T10:34:51.693' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (171, 7, 63, 20, CAST(N'2021-04-05T10:34:56.867' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (172, 6, 64, 20, CAST(N'2021-04-05T10:35:06.083' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (173, 6, 64, 20, CAST(N'2021-04-05T10:35:10.357' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (174, 2, 65, 20, CAST(N'2021-04-05T10:35:19.613' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (175, 2, 65, 20, CAST(N'2021-04-05T10:35:27.830' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (176, 1, 66, 20, CAST(N'2021-04-05T10:35:35.500' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (177, 1, 66, 20, CAST(N'2021-04-05T10:35:40.837' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (178, 2, 67, 20, CAST(N'2021-04-05T10:35:49.947' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (179, 2, 67, 20, CAST(N'2021-04-05T10:35:55.483' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (180, 2, 68, 20, CAST(N'2021-04-05T10:36:08.420' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (181, 2, 68, 20, CAST(N'2021-04-05T10:36:12.713' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (182, 7, 69, 20, CAST(N'2021-04-05T10:36:21.100' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (183, 7, 69, 20, CAST(N'2021-04-05T10:36:25.490' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (184, 2, 72, 20, CAST(N'2021-04-05T10:36:47.637' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (185, 2, 72, 20, CAST(N'2021-04-05T10:36:52.140' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (186, 2, 73, 20, CAST(N'2021-04-05T10:36:57.203' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (187, 2, 73, 20, CAST(N'2021-04-05T10:37:01.243' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (188, 2, 74, 20, CAST(N'2021-04-05T10:37:22.710' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (189, 2, 74, 20, CAST(N'2021-04-05T10:37:26.677' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (190, 2, 75, 20, CAST(N'2021-04-05T10:37:40.350' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (191, 2, 75, 20, CAST(N'2021-04-05T10:37:44.480' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (192, 2, 76, 20, CAST(N'2021-04-05T10:37:48.793' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (193, 2, 76, 20, CAST(N'2021-04-05T10:37:53.913' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (194, 2, 77, 20, CAST(N'2021-04-05T10:38:00.463' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (195, 2, 77, 20, CAST(N'2021-04-05T10:38:05.170' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (196, 2, 78, 20, CAST(N'2021-04-05T10:38:09.377' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (197, 2, 78, 20, CAST(N'2021-04-05T10:38:13.900' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (198, 1, 79, 20, CAST(N'2021-04-05T10:38:23.513' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (199, 1, 79, 20, CAST(N'2021-04-05T10:38:28.207' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (200, 7, 80, 20, CAST(N'2021-04-05T10:38:35.800' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (201, 7, 80, 20, CAST(N'2021-04-05T10:38:40.883' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (202, 1, 82, 20, CAST(N'2021-04-05T10:38:55.357' AS DateTime), NULL, N'40')
GO
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (203, 1, 82, 20, CAST(N'2021-04-05T10:38:59.987' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (204, 7, 83, 20, CAST(N'2021-04-05T10:39:07.000' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (205, 7, 83, 20, CAST(N'2021-04-05T10:39:11.090' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (206, 5, 84, 20, CAST(N'2021-04-05T10:39:17.353' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (207, 5, 84, 20, CAST(N'2021-04-05T10:39:21.037' AS DateTime), NULL, N'41')
SET IDENTITY_INSERT [dbo].[Product_Color] OFF
GO
ALTER TABLE [dbo].[Bill_Detail]  WITH CHECK ADD  CONSTRAINT [fk_Bill_BD] FOREIGN KEY([ID_Bill])
REFERENCES [dbo].[Bills] ([ID])
GO
ALTER TABLE [dbo].[Bill_Detail] CHECK CONSTRAINT [fk_Bill_BD]
GO
ALTER TABLE [dbo].[Bill_Detail]  WITH CHECK ADD  CONSTRAINT [fk_Pro_BD] FOREIGN KEY([ID_Product_Color])
REFERENCES [dbo].[Product_Color] ([ID])
GO
ALTER TABLE [dbo].[Bill_Detail] CHECK CONSTRAINT [fk_Pro_BD]
GO
ALTER TABLE [dbo].[Bills]  WITH CHECK ADD  CONSTRAINT [fk_cus_bill] FOREIGN KEY([ID_Customer])
REFERENCES [dbo].[Customer] ([ID])
GO
ALTER TABLE [dbo].[Bills] CHECK CONSTRAINT [fk_cus_bill]
GO
ALTER TABLE [dbo].[Cart]  WITH CHECK ADD  CONSTRAINT [fk_Cus_c] FOREIGN KEY([ID_Customer])
REFERENCES [dbo].[Customer] ([ID])
GO
ALTER TABLE [dbo].[Cart] CHECK CONSTRAINT [fk_Cus_c]
GO
ALTER TABLE [dbo].[Cart]  WITH CHECK ADD  CONSTRAINT [fk_Pro_C] FOREIGN KEY([ID_Product_Color])
REFERENCES [dbo].[Product_Color] ([ID])
GO
ALTER TABLE [dbo].[Cart] CHECK CONSTRAINT [fk_Pro_C]
GO
ALTER TABLE [dbo].[Favorites_list]  WITH CHECK ADD  CONSTRAINT [fk_Cus_F] FOREIGN KEY([ID_Customer])
REFERENCES [dbo].[Customer] ([ID])
GO
ALTER TABLE [dbo].[Favorites_list] CHECK CONSTRAINT [fk_Cus_F]
GO
ALTER TABLE [dbo].[Favorites_list]  WITH CHECK ADD  CONSTRAINT [fk_Pro_F] FOREIGN KEY([ID_Product])
REFERENCES [dbo].[Product] ([ID])
GO
ALTER TABLE [dbo].[Favorites_list] CHECK CONSTRAINT [fk_Pro_F]
GO
ALTER TABLE [dbo].[Imagee]  WITH CHECK ADD  CONSTRAINT [fk_Pro_Ima] FOREIGN KEY([ID_Product])
REFERENCES [dbo].[Product] ([ID])
GO
ALTER TABLE [dbo].[Imagee] CHECK CONSTRAINT [fk_Pro_Ima]
GO
ALTER TABLE [dbo].[New_Images]  WITH CHECK ADD  CONSTRAINT [fk_img_new] FOREIGN KEY([ID_New])
REFERENCES [dbo].[News] ([ID])
GO
ALTER TABLE [dbo].[New_Images] CHECK CONSTRAINT [fk_img_new]
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD  CONSTRAINT [fk_br_pro] FOREIGN KEY([ID_Brand])
REFERENCES [dbo].[Brand] ([ID])
GO
ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [fk_br_pro]
GO
ALTER TABLE [dbo].[Product_Color]  WITH CHECK ADD  CONSTRAINT [fk_Co_CoD] FOREIGN KEY([ID_Color])
REFERENCES [dbo].[Colorr] ([ID])
GO
ALTER TABLE [dbo].[Product_Color] CHECK CONSTRAINT [fk_Co_CoD]
GO
ALTER TABLE [dbo].[Product_Color]  WITH CHECK ADD  CONSTRAINT [fk_Pro_CoD] FOREIGN KEY([ID_Product])
REFERENCES [dbo].[Product] ([ID])
GO
ALTER TABLE [dbo].[Product_Color] CHECK CONSTRAINT [fk_Pro_CoD]
GO
